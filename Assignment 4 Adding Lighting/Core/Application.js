function Application()
{
}

Application.prototype.initialize = function(canvas)
{
    // Set canvas:
    this.canvas = canvas;
    this.aspectRatio = this.canvas.width / this.canvas.height;
    this.fieldOfViewY = 30.0;

    // Create graphics object:
    this.graphics = new Graphics();
    this.graphics.initialize(canvas);

    this.sceneObjects = {};
    this.matrices = {};
    this.coneCount = 0;
    this.cylinderCount = 0;
    this.sphereCount = 0;
    this.currentObject = null;

    this.lights = {};
    this.lightCount = 0;
    this.currentLight = null;

    // Create a scene:
    this.scene = new Scene();
    this.scene.initialize(this.graphics.gl, this.graphics.shaderManager);
    this.scene.setGlobalAmbientColor(this.graphics.gl, this.graphics.shaderManager, flatten([0.05, 0.05, 0.05]));
    this.scene.setLightLinearAttenuation(this.graphics.gl, this.graphics.shaderManager, 0.1);
    this.scene.setLightSquareAttenuation(this.graphics.gl, this.graphics.shaderManager, 0.01);

    // Setup a default camera:
    var cameraId = this.scene.addCamera([0.0, 0.0, 10.0]);
    this.scene.setActiveCamera(cameraId, this.graphics.gl, this.graphics.shaderManager);
    this.scene.setPerspectiveMode(this.graphics.gl, this.graphics.shaderManager, this.fieldOfViewY, this.aspectRatio);

    this.lastTime = null;
    this.elapsedTime = 0;
    this.animate = true;
};

Application.prototype.render = function()
{
    if(this.animate)
    {
        if(this.lastTime === null)
        {
            this.lastTime = Date.now();
            return;
        }

        var now = Date.now();
        var deltaTime = now - this.lastTime;
        this.lastTime = now;
        this.elapsedTime += deltaTime;

        this.scene.update(this.elapsedTime, deltaTime)
    }

    this.graphics.beginScene();
    this.scene.render(this.graphics.gl, this.graphics.shaderManager);
};

Application.prototype.translateCurrentObject = function(index, value)
{
    if(this.currentObject === null)
        return;

    var matrix = this.matrices[this.currentObject];
    matrix[0][index] = value;

    this.transformCurrentObject();
};

Application.prototype.rotateCurrentObject = function(axis, value)
{
    if(this.currentObject === null)
        return;

    var matrix = this.matrices[this.currentObject];
    matrix[1][axis] = value;

    this.transformCurrentObject();
};

Application.prototype.scaleCurrentObject = function(index, value)
{
    if(this.currentObject === null)
        return;

    var matrix = this.matrices[this.currentObject];
    matrix[2][index] = value;

    this.transformCurrentObject();
};

Application.prototype.transformCurrentObject = function()
{
    if(this.currentObject === null)
        return;

    var matrix = this.matrices[this.currentObject];

    var translateMatrix1 = translate(matrix[0][0], matrix[0][1], matrix[0][2]);
    var rotateXMatrix = rotate(matrix[1][0], [1, 0, 0]);
    var rotateYMatrix = rotate(matrix[1][1], [0, 1, 0]);
    var rotateZMatrix = rotate(matrix[1][2], [0, 0, 1]);
    var scaleMatrix = scalem(matrix[2][0], matrix[2][1], matrix[2][2]);

    var transform = mult(translateMatrix1, rotateXMatrix);
    transform = mult(transform, rotateYMatrix);
    transform = mult(transform, rotateZMatrix);
    transform = mult(transform, scaleMatrix);

    var objectId = this.sceneObjects[this.currentObject];
    this.scene.setSceneObjectTransform(objectId, transform);
};

Application.prototype.addCone = function(position, rotation)
{
    var id = "Cone" + this.coneCount++;

    var sceneId = this.scene.addCone();
    this.addSceneObject(id, sceneId, position, rotation);

    return id;
};

Application.prototype.addCylinder = function(position, rotation)
{
    var id = "Cylinder" + this.cylinderCount++;

    var sceneId = this.scene.addCylinder();
    this.addSceneObject(id, sceneId, position, rotation);

    return id;
};

Application.prototype.addSphere = function(position, rotation)
{
    var id = "Sphere" + this.sphereCount++;

    var sceneId = this.scene.addSphere();
    this.addSceneObject(id, sceneId, position, rotation);

    return id;
};

Application.prototype.addSceneObject = function(id, sceneId, position, rotation)
{
    this.matrices[id] = [
        [position[0], position[1], position[2]],
        [rotation[0], rotation[1], rotation[2]],
        [1.0, 1.0, 1.0]
    ];

    this.sceneObjects[id] = sceneId;
    this.currentObject = id;

    this.transformCurrentObject();
};

Application.prototype.removeCurrentSceneObject = function()
{
    if(this.currentObject === null)
        return false;

    var sceneId = this.sceneObjects[this.currentObject];
    this.sceneObjects[this.currentObject] = null;
    this.currentObject = null;

    this.scene.removeSceneObject(sceneId);

    return true;
};

Application.prototype.addLight = function(position, diffuseColor, specularColor, ambientColor)
{
    var id = "Light" + this.lightCount++;

    var sceneId = this.scene.addLight(this.graphics.gl, this.graphics.shaderManager, position, diffuseColor, specularColor, ambientColor);
    this.lights[id] = sceneId;
    this.currentLight = id;

    return id;
};

Application.prototype.getCurrentObjectMatrix = function()
{
    return this.matrices[this.currentObject];
};
Application.prototype.getCurrentLight = function()
{
    var sceneId = this.lights[this.currentLight];

    return this.scene.getLight(sceneId);
};

Application.prototype.setCurrentObject = function(id)
{
    this.currentObject = id;
};

Application.prototype.setCurrentLight = function(id)
{
    this.currentLight = id;
};

Application.prototype.setOrthogonalMode = function()
{
    this.scene.setOrthogonalMode(this.graphics.gl, this.graphics.shaderManager, this.aspectRatio);
};
Application.prototype.setPerspectiveMode = function()
{
    this.scene.setPerspectiveMode(this.graphics.gl, this.graphics.shaderManager, this.fieldOfViewY, this.aspectRatio);
};

Application.prototype.setCurrentLightPosition = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightPosition(sceneId, value);
};
Application.prototype.setCurrentLightDiffuseColor = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightDiffuseColor(sceneId, value);
};
Application.prototype.setCurrentLightSpecularColor = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightSpecularColor(sceneId, value);
};
Application.prototype.setCurrentLightAmbientColor = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightAmbientColor(sceneId, value);
};
Application.prototype.removeCurrentLight = function()
{
    if(this.currentLight === null)
        return false;

    var sceneId = this.lights[this.currentLight];
    this.lights[this.currentLight] = null;
    this.currentLight = null;

    this.scene.removeLight(this.graphics.gl, this.graphics.shaderManager, sceneId);

    return true;
};
Application.prototype.setCurrentLightActive = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightActive(this.graphics.gl, this.graphics.shaderManager, sceneId, value);
};

Application.prototype.setGlobalAmbientColor = function(value)
{
    this.scene.setGlobalAmbientColor(this.graphics.gl, this.graphics.shaderManager, value);
};

Application.prototype.setLightLinearAttenuation = function(value)
{
    this.scene.setLightLinearAttenuation(this.graphics.gl, this.graphics.shaderManager, value);
};

Application.prototype.setLightSquareAttenuation = function(value)
{
    this.scene.setLightSquareAttenuation(this.graphics.gl, this.graphics.shaderManager, value);
};

Application.prototype.setAnimate = function(value)
{
    this.animate = value;

    if(value === false)
        this.lastTime = null;
};

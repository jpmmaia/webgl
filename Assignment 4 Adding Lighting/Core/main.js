var application;

window.onload = function initialize()
{
    // Get canvas element:
    var canvas = document.getElementById("gl-canvas");

    // Initialize application:
    application = new Application();
    application.initialize(canvas);

    // Initialize user interface:
    initializePrimitiveForm();
    initializeLightForm();
    initializeOtherForm();

    createObject("Cone", [-2.0, 0.0, -2.0], [-90.0, 0.0, 0.0]);
    createObject("Sphere", [0.0, 0.0, -2.0], [0.0, 0.0, 0.0]);
    createObject("Cylinder", [2.0, 0.0, -2.0], [-90.0, 0.0, 0.0]);
    createLight([2.0, 2.0, -2.0], [0.0, 0.0, 0.7], [0.0, 0.0, 0.7], [0.0, 0.0, 0.02]);
    createLight([-2.0, -2.0, -2.0], [0.0, 0.7, 0.0], [0.0, 0.7, 0.0], [0.0, 0.02, 0.0]);

    onObjectChange();
    onLightChange();

    run();
};

function run()
{
    application.render();

    setTimeout(
        function()
        {
            requestAnimFrame(run);
        },
        30
    );
}

// <------------- INITIALIZE PRIMITIVE FORM ------------->

function initializePrimitiveForm()
{
    var createObjectInput = document.getElementById("CreateObject");
    createObjectInput.onclick = onCreateObjectClick;

    var objectInput = document.getElementById("Object");
    objectInput.onchange = onObjectChange;

    var deleteObjectInput = document.getElementById("DeleteObject");
    deleteObjectInput.onclick = onDeleteObjectClick;

    var translateXInput = document.getElementById("TranslateX");
    translateXInput.oninput = onTranslateXInput;
    var translateYInput = document.getElementById("TranslateY");
    translateYInput.oninput = onTranslateYInput;
    var translateZInput = document.getElementById("TranslateZ");
    translateZInput.oninput = onTranslateZInput;

    var rotateXInput = document.getElementById("RotateX");
    rotateXInput.oninput = onRotateXInput;
    var rotateYInput = document.getElementById("RotateY");
    rotateYInput.oninput = onRotateYInput;
    var rotateZInput = document.getElementById("RotateZ");
    rotateZInput.oninput = onRotateZInput;

    var scaleXInput = document.getElementById("ScaleX");
    scaleXInput.oninput = onScaleXInput;
    var scaleYInput = document.getElementById("ScaleY");
    scaleYInput.oninput = onScaleYInput;
    var scaleZInput = document.getElementById("ScaleZ");
    scaleZInput.oninput = onScaleZInput;
}

function onCreateObjectClick()
{
    var primitiveTypeInput = document.getElementById("PrimitiveType");
    var value = primitiveTypeInput.value;

    createObject(value, [0.0, 0.0, -2.0], [-90.0, 0.0, 0.0]);
}
function createObject(value, position, rotation)
{
    var id;
    if(value === "Cone")
        id = application.addCone(position, rotation);
    else if(value === "Cylinder")
        id = application.addCylinder(position, rotation);
    else if(value === "Sphere")
        id = application.addSphere(position, rotation);

    var option = document.createElement("option");
    option.text = id;
    option.value = id;

    var objectInput = document.getElementById("Object");
    objectInput.options.add(option);
    objectInput.value = id;

    onObjectChange();
}

function onObjectChange()
{
    var objectInput = document.getElementById("Object");
    var value = objectInput.value;

    if(value === "")
        return;

    application.setCurrentObject(value);

    // Update interface values:
    var matrix = application.getCurrentObjectMatrix();

    var translateXInput = document.getElementById("TranslateX");
    translateXInput.value = matrix[0][0];

    var translateYInput = document.getElementById("TranslateY");
    translateYInput.value = matrix[0][1];

    var translateZInput = document.getElementById("TranslateZ");
    translateZInput.value = matrix[0][2];

    var rotateXInput = document.getElementById("RotateX");
    rotateXInput.value = matrix[1][0];

    var rotateYInput = document.getElementById("RotateY");
    rotateYInput.value = matrix[1][1];

    var rotateZInput = document.getElementById("RotateZ");
    rotateZInput.value = matrix[1][2];

    var scaleXInput = document.getElementById("ScaleX");
    scaleXInput.value = matrix[2][0];

    var scaleYInput = document.getElementById("ScaleY");
    scaleYInput.value = matrix[2][1];

    var scaleZInput = document.getElementById("ScaleZ");
    scaleZInput.value = matrix[2][2];
}

function onDeleteObjectClick()
{
    if(application.removeCurrentSceneObject() === true)
    {
        var element = document.getElementById("Object");
        element.options.remove(element.selectedIndex);

        value = element.value;
        if(value === "")
            value = null;

        application.setCurrentObject(value);
        onObjectChange();
    }
}

function onTranslateXInput()
{
    var translateXInput = document.getElementById("TranslateX");
    var value = translateXInput.value;
    application.translateCurrentObject(0, value);
}
function onTranslateYInput()
{
    var translateYInput = document.getElementById("TranslateY");
    var value = translateYInput.value;
    application.translateCurrentObject(1, value);
}
function onTranslateZInput()
{
    var translateZInput = document.getElementById("TranslateZ");
    var value = translateZInput.value;
    application.translateCurrentObject(2, value);
}

function onRotateXInput()
{
    var element = document.getElementById("RotateX");
    var value = element.value;
    application.rotateCurrentObject(0, value);
}
function onRotateYInput()
{
    var element = document.getElementById("RotateY");
    var value = element.value;
    application.rotateCurrentObject(1, value);
}
function onRotateZInput()
{
    var element = document.getElementById("RotateZ");
    var value = element.value;
    application.rotateCurrentObject(2, value);
}

function onScaleXInput()
{
    var element = document.getElementById("ScaleX");
    var value = element.value;
    application.scaleCurrentObject(0, value);
}
function onScaleYInput()
{
    var element = document.getElementById("ScaleY");
    var value = element.value;
    application.scaleCurrentObject(1, value);
}
function onScaleZInput()
{
    var element = document.getElementById("ScaleZ");
    var value = element.value;
    application.scaleCurrentObject(2, value);
}


// <------------- INITIALIZE LIGHT FORM ------------->

function initializeLightForm()
{
    var createLightInput = document.getElementById("CreatePointLight");
    createLightInput.onclick = onCreatePointLightClick;

    var lightInput = document.getElementById("Light");
    lightInput.onchange = onLightChange;

    var deleteLightInput = document.getElementById("DeleteLight");
    deleteLightInput.onclick = onDeletetLightClick;

    var activeLightInput = document.getElementById("ActiveLight");
    activeLightInput.onchange = onActivetLightChange;

    var translateLightXInput = document.getElementById("TranslateLightX");
    translateLightXInput.oninput = onTranslateLightXInput;
    var translateLightYInput = document.getElementById("TranslateLightY");
    translateLightYInput.oninput = onTranslateLightYInput;
    var translateLightZInput = document.getElementById("TranslateLightZ");
    translateLightZInput.oninput = onTranslateLightZInput;

    var lightDiffuseColorInput = document.getElementById("LightDiffuseColor");
    lightDiffuseColorInput.oninput = onLightDiffuseColorInput;
    var lightSpecularColorInput = document.getElementById("LightSpecularColor");
    lightSpecularColorInput.oninput = onLightSpecularColorInput;
    var lightAmbientColorInput = document.getElementById("LightAmbientColor");
    lightAmbientColorInput.oninput = onLightAmbientColorInput;

    document.getElementById("LightLinearAttenuation").oninput = onLightLinearAttenuationInput;
    document.getElementById("LightSquareAttenuation").oninput = onLightSquareAttenuationInput;

    document.getElementById("AnimateLights").onchange = onAnimateLightsChange;
}

function onCreatePointLightClick()
{
    var position = [];
    position[0] = parseFloat(document.getElementById("TranslateLightX").value);
    position[1] = parseFloat(document.getElementById("TranslateLightY").value);
    position[2] = parseFloat(document.getElementById("TranslateLightZ").value);

    var diffuseColor = hexToColor(document.getElementById("LightDiffuseColor").value);
    var specularColor = hexToColor(document.getElementById("LightSpecularColor").value);
    var ambientColor = hexToColor(document.getElementById("LightAmbientColor").value);

    createLight(position, diffuseColor, specularColor, ambientColor);
}
function createLight(position, diffuseColor, specularColor, ambientColor)
{
    var id = application.addLight(position, diffuseColor, specularColor, ambientColor);

    var option = document.createElement("option");
    option.text = id;
    option.value = id;

    var lightInput = document.getElementById("Light");
    lightInput.options.add(option);
    lightInput.value = id;
}

function onLightChange()
{
    var lightInput = document.getElementById("Light");
    var value = lightInput.value;
    if(value === "")
        return;

    application.setCurrentLight(value);

    // Update interface values:
    var light = application.getCurrentLight();

    var position = light.getPosition();
    document.getElementById("TranslateLightX").value = position[0];
    document.getElementById("TranslateLightY").value = position[1];
    document.getElementById("TranslateLightZ").value = position[2];

    document.getElementById("LightDiffuseColor").value = colorToHex(light.getDiffuseColor());
    document.getElementById("LightSpecularColor").value = colorToHex(light.getSpecularColor());
    document.getElementById("LightAmbientColor").value = colorToHex(light.getAmbientColor());
    document.getElementById("ActiveLight").checked = light.isActive();
}

function onDeletetLightClick()
{
    if(application.removeCurrentLight())
    {
        var element = document.getElementById("Light");
        element.options.remove(element.selectedIndex);

        value = element.value;
        if(value === "")
            value = null;

        application.setCurrentLight(value);

        onLightChange();
    }
}

function onActivetLightChange()
{
    application.setCurrentLightActive(document.getElementById("ActiveLight").checked);
}

function onTranslateLightXInput()
{
    var light = application.getCurrentLight();
    var position = light.getPosition();
    position[0] = document.getElementById("TranslateLightX").value;

    application.setCurrentLightPosition(position);
}
function onTranslateLightYInput()
{
    var light = application.getCurrentLight();
    var position = light.getPosition();
    position[1] = document.getElementById("TranslateLightY").value;

    application.setCurrentLightPosition(position);
}
function onTranslateLightZInput()
{
    var light = application.getCurrentLight();
    var position = light.getPosition();
    position[2] = document.getElementById("TranslateLightZ").value;

    application.setCurrentLightPosition(position);
}

function onLightDiffuseColorInput()
{
    var color = hexToColor(document.getElementById("LightDiffuseColor").value);
    application.setCurrentLightDiffuseColor(color);
}
function onLightSpecularColorInput()
{
    var color = hexToColor(document.getElementById("LightSpecularColor").value);
    application.setCurrentLightSpecularColor(color);
}
function onLightAmbientColorInput()
{
    var color = hexToColor(document.getElementById("LightAmbientColor").value);
    application.setCurrentLightAmbientColor(color);
}

function onLightLinearAttenuationInput()
{
    application.setLightLinearAttenuation(document.getElementById("LightLinearAttenuation").value);
}

function onLightSquareAttenuationInput()
{
    application.setLightSquareAttenuation(document.getElementById("LightSquareAttenuation").value);
}

// <------------- INITIALIZE OTHER FORM ------------->

function initializeOtherForm()
{
    var orthogonalModeInput = document.getElementById("OrthogonalMode");
    orthogonalModeInput.onclick = onOrthogonalModeClick;
    var perspectiveModeInput = document.getElementById("PerspectiveMode");
    perspectiveModeInput.onclick = onPerspectiveModeClick;

    var globalAmbientColorInput = document.getElementById("GlobalAmbientColor");
    globalAmbientColorInput.oninput = onGlobalAmbientColorInput;
}

function onOrthogonalModeClick()
{
    application.setOrthogonalMode();
}
function onPerspectiveModeClick()
{
    application.setPerspectiveMode();
}

function onGlobalAmbientColorInput()
{
    application.setGlobalAmbientColor(hexToColor(document.getElementById("GlobalAmbientColor").value));
}

function onAnimateLightsChange()
{
    application.setAnimate(document.getElementById("AnimateLights").checked);
}

// <------------- HELPER FUNCTIONS ------------->

function hexToColor(hex)
{
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if(!result)
        return [0.0, 0.0, 0.0];

    return [
        parseInt(result[1], 16) / 255.0,
        parseInt(result[2], 16) / 255.0,
        parseInt(result[3], 16) / 255.0
    ];
}

function colorComponentToHex(component)
{
    var hex = Math.round(component * 255).toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function colorToHex(color)
{
    return "#" + colorComponentToHex(color[0]) + colorComponentToHex(color[1]) + colorComponentToHex(color[2]);
}
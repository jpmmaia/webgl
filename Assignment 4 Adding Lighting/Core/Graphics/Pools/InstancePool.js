function InstancePool(maxSize)
{
    this.instances = [];
    this.instances.length = maxSize;
    this.maxInstanceCount = maxSize;

    for(var i = 0; i < maxSize; i++)
        this.createInstance(i);

    this.firstAvailable = 0;
}

InstancePool.prototype.renderInstance = function(gl, modelPool, instanceIndex)
{
    var instance = this.instances[instanceIndex];

    if(instance.active)
        modelPool.renderModel(gl, instance.modelIndex);
};

InstancePool.prototype.addConeInstance = function()
{
    return this.addInstance(0);
};

InstancePool.prototype.addCylinderInstance = function()
{
    return this.addInstance(1);
};

InstancePool.prototype.addSphereInstance = function()
{
    return this.addInstance(2);
};

InstancePool.prototype.addInstance = function(modelIndex)
{
    if(this.firstAvailable === null)
        throw new Error("Model Pool is full!");

    // Get instance and change first available to the the next available index:
    var instanceIndex = this.firstAvailable;
    var instance = this.instances[instanceIndex];
    this.firstAvailable = instance.nextAvailableIndex;

    // Set instance to active mode:
    instance.modelIndex = modelIndex;
    instance.active = true;

    return instanceIndex;
};
InstancePool.prototype.removeInstance = function(index)
{
    var instance = this.instances[index];

    // Set instance to inactive mode:
    instance.active = false;
    instance.modelIndex = null;

    // Set first available index to the removed instance index:
    instance.nextAvailableIndex = this.firstAvailable;
    this.firstAvailable = index;
};

InstancePool.prototype.createInstance = function(index)
{
    var instance = {};
    instance.nextAvailableIndex = index + 1;
    if(index === this.maxInstanceCount - 1)
        instance.nextAvailableIndex = null;

    instance.active = false;
    instance.modelIndex = null;

    this.instances[index] = instance;
};
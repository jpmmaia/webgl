function TransformPool(maxSize)
{
    this.transforms = [];
    this.transforms.length = maxSize;

    for(var i = 0; i < maxSize; i++)
        this.transforms[i] = new TransformComponent();

    for(var i = 0; i < maxSize; i++)
        this.transforms[i].nextActiveIndex = i + 1;
    this.transforms[maxSize - 1].nextActiveIndex = null;

    this.firstActiveIndex = 0;
}

TransformPool.prototype.addTransform = function(value)
{
    if(this.firstActiveIndex === null)
        throw new Error("The pool is full!");

    // Get first active index:
    var index = this.firstActiveIndex;

    // Get first active transform:
    var transform = this.transforms[index];

    // Update first active index and set nextActiveIndex to null:
    this.firstActiveIndex = transform.nextActiveIndex;
    transform.nextActiveIndex = null;

    // Initialize transform:
    transform.initialize(index);
    transform.setTransform(value);

    return index;
};
TransformPool.prototype.removeTransform = function(index)
{
    // Get transform which will be removed:
    var transform = this.transforms[index];

    // Update next active index and first active index:
    transform.nextActiveIndex = this.firstActiveIndex;
    this.firstActiveIndex = index;

    // Shutdown transform:
    transform.shutdown();
};
TransformPool.prototype.setTransform = function(index, value)
{
    // Get transform which will be set:
    var transform = this.transforms[index];

    // Update transform value:
    transform.setTransform(value);
};

TransformPool.prototype.renderTransform = function(gl, shaderManager, index)
{
    // Get transform:
    var transform = this.transforms[index];

    // Render transform:
    if(transform.isActive())
        transform.render(gl, shaderManager);
};
function LightsPool(maxSize)
{
    this.lights = [];
    this.lights.length = maxSize;

    for(var i = 0; i < maxSize; i++)
        this.lights[i] = new PointLight();

    for(var i = 0; i < maxSize - 1; i++)
        this.lights[i].nextActiveIndex = (i + 1);
    this.lights[maxSize - 1].nextActiveIndex = null;

    this.firstActiveIndex = 0;
}

LightsPool.prototype.addLight = function(gl, shaderManager, position, diffuseColor, specularColor, ambientColor)
{
    if(this.firstActiveIndex === null)
        throw new Error("The pool is full!");

    // Get first active index:
    var index = this.firstActiveIndex;

    // Get first active light:
    var light = this.lights[index];

    // Update first active index and set nextActiveIndex to null:
    this.firstActiveIndex = light.nextActiveIndex;
    light.nextActiveIndex = null;

    // Initialize light:
    light.initialize(gl, shaderManager, index, position, diffuseColor, specularColor, ambientColor);

    return index;
};
LightsPool.prototype.removeLight = function(gl, shaderManager, index)
{
    // Get light which will be removed:
    var light = this.lights[index];

    // Update next active index and first active index:
    light.nextActiveIndex = this.firstActiveIndex;
    this.firstActiveIndex = index;

    // Shutdown light:
    light.shutdown(gl, shaderManager);
};

LightsPool.prototype.render = function(gl, shaderManager)
{
    for(var i = 0; i < this.lights.length; i++)
    {
        var light = this.lights[i];

        if(light.isActive())
            light.render(gl, shaderManager);
    }
};

LightsPool.prototype.getLight = function(index)
{
    return this.lights[index];
};

LightsPool.prototype.setLight = function(index, position, diffuseColor, specularColor, ambientColor)
{
    // Get light which will be set:
    var light = this.lights[index];

    // Update light parameters:
    light.setParameters(position, diffuseColor, specularColor, ambientColor);
};

LightsPool.prototype.setLightPosition = function(index, value)
{
    // Get light which will be set:
    var light = this.lights[index];

    // Update light position:
    light.setPosition(value);
};
LightsPool.prototype.setLightDiffuseColor = function(index, value)
{
    // Get light which will be set:
    var light = this.lights[index];

    // Update light diffuse color:
    light.setDiffuseColor(value);
};
LightsPool.prototype.setLightSpecularColor = function(index, value)
{
    // Get light which will be set:
    var light = this.lights[index];

    // Update light specular color:
    light.setSpecularColor(value);
};
LightsPool.prototype.setLightAmbientColor = function(index, value)
{
    // Get light which will be set:
    var light = this.lights[index];

    // Update light ambient color:
    light.setAmbientColor(value);
};
LightsPool.prototype.setLightActive = function(gl, shaderManager, index, value)
{
    // Get light which will be set:
    var light = this.lights[index];

    // Update light active variable:
    light.setActive(gl, shaderManager, value);
};
function ModelPool()
{
    this.coneModel = new Cone();
    this.cylinderModel = new Cylinder();
    this.sphereModel = new Sphere();
}

ModelPool.prototype.initialize = function(gl, shaderManager)
{
    this.coneModel.initialize(gl, 32, 16, shaderManager.lightShader.position, shaderManager.lightShader.normal);
    this.cylinderModel.initialize(gl, 32, 16, shaderManager.lightShader.position, shaderManager.lightShader.normal);
    this.sphereModel.initialize(gl, 32, 16, shaderManager.lightShader.position, shaderManager.lightShader.normal);
};
ModelPool.prototype.shutdown = function(gl)
{
    this.sphereModel.shutdown(gl);
    this.cylinderModel.shutdown(gl);
    this.coneModel.shutdown(gl);
};

ModelPool.prototype.renderModel = function(gl, modelIndex)
{
    switch(modelIndex)
    {
        case 0:
            this.coneModel.render(gl);
            break;
        case 1:
            this.cylinderModel.render(gl);
            break;
        case 2:
            this.sphereModel.render(gl);
            break;
    }
};
function ObjectPool(maxLightCount, maxModelCount)
{
    this.modelPool = new ModelPool();

    // Initialize lights:
    this.lightPool = new LightsPool(maxLightCount);
    this.lightTransformPool = new TransformPool(maxLightCount);
    this.lightInstancePool = new InstancePool(maxLightCount);
    this.maxLightCount = maxLightCount;

    // Initialize scene objects:
    this.sceneObjectTransformPool = new TransformPool(maxModelCount);
    this.sceneObjectPool = new InstancePool(maxModelCount);
    this.maxModelCount = maxModelCount;
}

ObjectPool.prototype.initialize = function(gl, shaderManager)
{
    this.modelPool.initialize(gl, shaderManager);
};
ObjectPool.prototype.shutdown = function(gl)
{
    this.modelPool.shutdown(gl);
};

ObjectPool.prototype.addLight = function(gl, shaderManager, position, diffuseColor, specularColor, ambientColor)
{
    var index = this.lightPool.addLight(gl, shaderManager, position, diffuseColor, specularColor, ambientColor);
    this.lightTransformPool.addTransform(
        PointLight.createTransform(position)
    );
    this.lightInstancePool.addSphereInstance();

    return index;
};
ObjectPool.prototype.removeLight = function(gl, shaderManager, index)
{
    this.lightInstancePool.removeInstance(index);
    this.lightTransformPool.removeTransform(index);
    this.lightPool.removeLight(gl, shaderManager, index);
};
ObjectPool.prototype.renderLights = function(gl, shaderManager)
{
    this.lightPool.render(gl, shaderManager);

    for(var i = 0; i < this.maxLightCount; i++)
    {
        this.lightTransformPool.renderTransform(gl, shaderManager, i);
        this.lightInstancePool.renderInstance(gl, this.modelPool, i);
    }
};
ObjectPool.prototype.setLightPosition = function(index, value)
{
    this.lightPool.setLightPosition(index, value);
    this.lightTransformPool.setTransform(
        index,
        PointLight.createTransform(value)
    );
};
ObjectPool.prototype.setLightDiffuseColor = function(index, value)
{
    this.lightPool.setLightDiffuseColor(index, value);
};
ObjectPool.prototype.setLightSpecularColor = function(index, value)
{
    this.lightPool.setLightSpecularColor(index, value);
};
ObjectPool.prototype.setLightAmbientColor = function(index, value)
{
    this.lightPool.setLightAmbientColor(index, value);
};
ObjectPool.prototype.getLight = function(index)
{
    return this.lightPool.getLight(index);
};
ObjectPool.prototype.setLightActive = function(gl, shaderManager, index, value)
{
    this.lightPool.setLightActive(gl, shaderManager, index, value);
};

ObjectPool.prototype.addCone = function(transform)
{
    var index = this.sceneObjectPool.addConeInstance();
    this.sceneObjectTransformPool.addTransform(transform);

    return index;
};
ObjectPool.prototype.addCylinder = function(transform)
{
    var index = this.sceneObjectPool.addCylinderInstance();
    this.sceneObjectTransformPool.addTransform(transform);

    return index;
};
ObjectPool.prototype.addSphere = function(transform)
{
    var index = this.sceneObjectPool.addSphereInstance();
    this.sceneObjectTransformPool.addTransform(transform);

    return index;
};
ObjectPool.prototype.setSceneObjectTransform = function(index, transform)
{
    this.sceneObjectTransformPool.setTransform(index, transform);
};
ObjectPool.prototype.removeSceneObject = function(index)
{
    this.sceneObjectTransformPool.removeTransform(index);
    this.sceneObjectPool.removeInstance(index);
};

ObjectPool.prototype.renderSceneObjects = function(gl, shaderManager)
{
    for(var i = 0; i < this.maxModelCount; i++)
    {
        this.sceneObjectTransformPool.renderTransform(gl, shaderManager, i);
        this.sceneObjectPool.renderInstance(gl, this.modelPool, i);
    }
};
function Scene()
{
    this.maxLightCount = 4;
    this.maxSceneObjectCount = 50;
    this.sceneGraph = new SceneGraph();
    this.objectPool = new ObjectPool(this.maxLightCount, this.maxSceneObjectCount);

    this.deltaAngle = 0.1;
}

Scene.prototype.initialize = function(gl, shaderManager)
{
    // Set clear color:
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // Initialize scene graph:
    this.sceneGraph.initialize();

    // Set light shader:
    shaderManager.setLightShader(gl);
    this.objectCount = 0;

    // Create an hash table to hold cameras:
    this.cameras = [];

    // Initialize object pool:
    this.objectPool.initialize(gl, shaderManager);
};

Scene.prototype.shutdown = function(gl)
{
    this.objectPool.shutdown(gl);
};

Scene.prototype.update = function(elapsedTime, deltaTime)
{
    for(var i = 0; i < this.maxLightCount; i++)
    {
        var light = this.objectPool.getLight(i);
        if(!light.isActive())
            continue;

        var angle = this.deltaAngle * elapsedTime / 1000.0;

        var position = light.getPosition();
        position[0] = (Math.cos(radians(angle)) * position[0] - Math.sin(radians(angle)) * position[2]);
        position[2] = (Math.sin(radians(angle)) * position[0] + Math.cos(radians(angle)) * position[2]);

        this.objectPool.setLightPosition(i, flatten(position));
    }
};

Scene.prototype.render = function(gl, shaderManager)
{
    // Set color shader:
    shaderManager.setLightShader(gl);

    // Set view matrix:
    shaderManager.setViewMatrix(gl, flatten(this.currentCamera.getViewMatrix()));

    // Set shader parameters:
    shaderManager.setCameraPosition(gl, flatten(this.currentCamera.getPosition()));

    // Render all lights:
    this.objectPool.renderLights(gl, shaderManager);

    // Render all scene objects:
    this.objectPool.renderSceneObjects(gl, shaderManager);
};

Scene.prototype.addCamera = function(position)
{
    // Create an ID:
    var id = this.objectCount++;

    // Create a camera component:
    var cameraComponent = new CameraComponent();
    cameraComponent.initialize(id);

    // Set position:
    cameraComponent.translate(position[0], position[1], position[2]);

    // Add camera to hash table:
    this.cameras[id] = cameraComponent;

    return id;
};

Scene.prototype.addSphere = function()
{
    return this.objectPool.addSphere(mat4(1));
};
Scene.prototype.addCylinder = function()
{
    return this.objectPool.addCylinder(mat4(1));
};
Scene.prototype.addCone = function()
{
    return this.objectPool.addCone(mat4(1));
};
Scene.prototype.removeSceneObject = function(index)
{
    this.objectPool.removeSceneObject(index);
};

Scene.prototype.addLight = function(gl, shaderManager, position, diffuseColor, specularColor, ambientColor)
{
    return this.objectPool.addLight(gl, shaderManager, position, diffuseColor, specularColor, ambientColor);
};
Scene.prototype.removeLight = function(gl, shaderManager, index)
{
    this.objectPool.removeLight(gl, shaderManager, index);
};

Scene.prototype.setActiveCamera = function(id, gl, shaderManager)
{
    this.currentCamera = this.cameras[id];

    // Setup projection matrix:
    shaderManager.setProjectionMatrix(gl, flatten(this.currentCamera.getProjectionMatrix()));
};
Scene.prototype.setOrthogonalMode = function(gl, shaderManager, aspectRatio)
{
    this.currentCamera.setOrthogonal(-3.0 * aspectRatio, 3.0 * aspectRatio, -3.0, 3.0, -20.0, 20.0);
    shaderManager.setProjectionMatrix(gl, flatten(this.currentCamera.getProjectionMatrix()));

    this.render(gl, shaderManager);
};
Scene.prototype.setPerspectiveMode = function(gl, shaderManager, fieldOfViewY, aspectRatio)
{
    this.currentCamera.setPerspective(fieldOfViewY, aspectRatio, 0.1, 20.0);
    shaderManager.setProjectionMatrix(gl, flatten(this.currentCamera.getProjectionMatrix()));
};

Scene.prototype.getLight = function(id)
{
    return this.objectPool.getLight(id);
};

Scene.prototype.setLightPosition = function(id, value)
{
    this.objectPool.setLightPosition(id, value);
};
Scene.prototype.setLightDiffuseColor = function(id, value)
{
    this.objectPool.setLightDiffuseColor(id, value);
};
Scene.prototype.setLightSpecularColor = function(id, value)
{
    this.objectPool.setLightSpecularColor(id, value);
};
Scene.prototype.setLightAmbientColor = function(id, value)
{
    this.objectPool.setLightAmbientColor(id, value);
};
Scene.prototype.setLightActive = function(gl, shaderManager, index, value)
{
    this.objectPool.setLightActive(gl, shaderManager, index, value);
};

Scene.prototype.setSceneObjectTransform = function(id, value)
{
    this.objectPool.setSceneObjectTransform(id, value);
};

Scene.prototype.setGlobalAmbientColor = function(gl, shaderManager, value)
{
    shaderManager.setGlobalAmbientColor(gl, value);
};

Scene.prototype.setLightLinearAttenuation = function(gl, shaderManager, value)
{
    shaderManager.setLinearAttenuation(gl, value);
};

Scene.prototype.setLightSquareAttenuation = function(gl, shaderManager, value)
{
    shaderManager.setSquareAttenuation(gl, value);
};
function TransformComponent()
{
}

TransformComponent.prototype.initialize = function(id)
{
    this.id = id;
    this.transform = mat4(1);
    this.active = true;
};

TransformComponent.prototype.shutdown = function()
{
    this.active = false;
};

TransformComponent.prototype.render = function(gl, shaderManager)
{
    // Set model matrix in the shader:
    shaderManager.setModelMatrix(gl, flatten(this.transform));
};

TransformComponent.prototype.getId = function()
{
    return this.id;
};

TransformComponent.prototype.getTransform = function()
{
    return this.transform;
};

TransformComponent.prototype.setTransform = function(value)
{
    this.transform = value;
};

TransformComponent.prototype.isActive = function()
{
    return this.active;
};
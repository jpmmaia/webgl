function AbstractModel()
{
}

AbstractModel.prototype.initialize = function(gl, positionAttribLocation, colorAttribLocation, vertices, colors)
{
    this.positionAttribLocation = positionAttribLocation;
    this.colorAttribLocation = colorAttribLocation;

    // Create vertex buffer:
    this.vertexBufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBufferId);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    // Create color buffer:
    this.colorBufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBufferId);
    gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);
};

AbstractModel.prototype.shutdown = function(gl)
{
    // Release the color buffer:
    gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBufferId);
    gl.disableVertexAttribArray(this.colorAttribLocation);
    gl.deleteBuffer(this.colorBufferId);

    // Release the vertex buffer:
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBufferId);
    gl.disableVertexAttribArray(this.positionAttribLocation);
    gl.deleteBuffer(this.vertexBufferId);
};

AbstractModel.prototype.render = function(gl)
{
    // Specify position:
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBufferId);
    gl.enableVertexAttribArray(this.positionAttribLocation);
    gl.vertexAttribPointer(this.positionAttribLocation, 3, gl.FLOAT, false, 0, 0);

    // Specify color:
    gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBufferId);
    gl.enableVertexAttribArray(this.colorAttribLocation);
    gl.vertexAttribPointer(this.colorAttribLocation, 4, gl.FLOAT, false, 0, 0);
};

AbstractModel.prototype.setVertex = function(gl, position, color, index)
{
    // Set vertex position:
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBufferId);
    gl.bufferSubData(gl.ARRAY_BUFFER, 12 * index, position);

    // Set vertex color:
    gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBufferId);
    gl.bufferSubData(gl.ARRAY_BUFFER, 16 * index, color);
};
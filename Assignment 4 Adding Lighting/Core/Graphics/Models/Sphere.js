function Sphere()
{
}

Sphere.prototype.initialize = function(gl, segments, rings, positionAttributeLocation, normalAttributeLocation)
{
    this.initializeVertexBuffer(gl, segments, rings, positionAttributeLocation);
    this.initializeNormalBuffer(gl, normalAttributeLocation);
    this.initializeIndexBuffer(gl, segments, rings);
};

Sphere.prototype.shutdown = function(gl)
{
    this.indexBuffer.shutdown(gl);
    this.normalBuffer.shutdown(gl);
    this.vertexBuffer.shutdown(gl);
};

Sphere.prototype.render = function(gl)
{
    this.vertexBuffer.bind(gl);
    this.normalBuffer.bind(gl);

    this.indexBuffer.draw(gl, gl.TRIANGLES);
};

Sphere.prototype.initializeVertexBuffer = function(gl, segments, rings, positionAttributeLocation)
{
    var maxVertexCount = segments * rings + 1;
    this.vertices = [];
    this.vertices.length = maxVertexCount * 3;
    this.vertexCount = 0;

    // Add top vertex:
    this.vertices[0] = 0.0;
    this.vertices[1] = 0.0;
    this.vertices[2] = -1.0;
    this.vertexCount++;

    // Add middle vertices:
    var deltaPhi = 2 * Math.PI / segments;
    var deltaTheta = Math.PI / rings;
    for(var i = 0; i < segments; i++)
    {
        var phi = i * deltaPhi;
        var sinPhi = Math.sin(phi);
        var cosPhi = Math.cos(phi);

        for(var j = 1; j <= rings; j++)
        {
            var theta = j * deltaTheta;
            var sinTheta = Math.sin(theta);
            var cosTheta = Math.cos(theta);

            var vertexIndex = this.vertexCount * 3;
            this.vertices[vertexIndex] = sinTheta * cosPhi;
            this.vertices[vertexIndex + 1] = sinTheta * sinPhi;
            this.vertices[vertexIndex + 2] = -cosTheta;

            this.vertexCount++;
        }
    }

    // Initialize vertex buffer:
    this.vertexBuffer = new GLBuffer();
    this.vertexBuffer.initialize(gl, gl.ARRAY_BUFFER, flatten(this.vertices), this.vertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 3, positionAttributeLocation);
};

Sphere.prototype.initializeNormalBuffer = function(gl, normalAttributeLocation)
{
    var normals = [];
    normals.length = this.vertices.length;

    for(var i = 0; i < normals.length; i++)
        normals[i] = this.vertices[i];

    // Initialize normal buffer:
    this.normalBuffer = new GLBuffer();
    this.normalBuffer.initialize(gl, gl.ARRAY_BUFFER, flatten(normals), this.vertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 3, normalAttributeLocation);
};

Sphere.prototype.initializeIndexBuffer = function(gl, segments, rings)
{
    var triangleCount = segments * (1 + 2 * (rings - 1));

    this.indices = [];
    this.indices.length = triangleCount * 3;

    var index = 0;
    for(var i = 0; i < segments; i++)
    {
        this.indices[index++] = -1;
        this.indices[index++] = ((i + 1) * rings) % (rings * segments);
        this.indices[index++] = i * rings;
    }

    for(var i = 0; i < segments; i++)
    {
        this.indices[index++] = i * rings;
        this.indices[index++] = ((i + 1) * rings) % (rings * segments);
        this.indices[index++] = i * rings + 1;

        this.indices[index++] = i * rings + 1;
        this.indices[index++] = ((i + 1) * rings) % (rings * segments);
        this.indices[index++] = ((i + 1) * rings + 1) % (rings * segments);
    }

    for (var i = 0; i < 2 * segments * (rings - 2) * 3; i++)
       this.indices[9 * segments + i] = this.indices[3 * segments + i] + 1;

    for (var i = 0; i < this.indices.length; i++)
        this.indices[i] = this.indices[i] + 1;

    // Initialize index buffer:
    this.indexBuffer = new GLIndexBuffer();
    this.indexBuffer.initialize(gl, new Uint16Array(this.indices));
};
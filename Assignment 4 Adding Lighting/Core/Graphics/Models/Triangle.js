function Triangle()
{
}

Triangle.prototype.initialize = function(gl, positionAttributeLocation, normalAttributeLocation)
{
    var radius = 0.5;
    var angle1 = Math.PI / 2.0;
    var angle2 = 7.0 * Math.PI / 6.0;
    var angle3 = 11.0 * Math.PI / 6.0;

    // Create vertex buffer:
    var vertices =
        [
            radius * Math.cos(angle1), radius * Math.sin(angle1), 0.0,
            radius * Math.cos(angle2), radius * Math.sin(angle2), 0.0,
            radius * Math.cos(angle3), radius * Math.sin(angle3), 0.0
        ];
    this.vertexBuffer = new GLBuffer();
    this.vertexBuffer.initialize(gl, gl.ARRAY_BUFFER, flatten(vertices), 3, gl.STATIC_DRAW, gl.FLOAT, 4, 3, positionAttributeLocation);

    // Create normal buffer:
    var normals =
        [
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0
        ];
    this.normalBuffer = new GLBuffer();
    this.normalBuffer.initialize(gl, gl.ARRAY_BUFFER, flatten(normals), 3, gl.STATIC_DRAW, gl.FLOAT, 4, 3, normalAttributeLocation);

    // Create index buffer:
    var indices =
        [
            0, 1, 2
        ];
    this.indexBuffer = new GLIndexBuffer();
    this.indexBuffer.initialize(gl, new Uint16Array(indices));
};

Triangle.prototype.shutdown = function(gl)
{
    this.indexBuffer.shutdown(gl);
    this.normalBuffer.shutdown(gl);
    this.vertexBuffer.shutdown(gl);
};

Triangle.prototype.render = function(gl)
{
    this.vertexBuffer.bind(gl);
    this.normalBuffer.bind(gl);

    this.indexBuffer.draw(gl, gl.TRIANGLES);
};
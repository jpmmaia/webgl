function Cone()
{
}

Cone.prototype.initialize = function(gl, segments, rings, positionAttributeLocation, colorAttributeLocation)
{
    this.initializeVertexBuffer(gl, segments, rings, positionAttributeLocation);
    this.initializeColorBuffer(gl, colorAttributeLocation);
    this.initializeIndexBuffer(gl, segments, rings);
};

Cone.prototype.shutdown = function(gl)
{
    this.indexBuffer.shutdown(gl);
    this.colorBuffer.shutdown(gl);
    this.vertexBuffer.shutdown(gl);
};

Cone.prototype.render = function(gl)
{
    this.vertexBuffer.bind(gl);
    this.colorBuffer.bind(gl);

    this.indexBuffer.draw(gl, gl.TRIANGLES);
};

Cone.prototype.initializeVertexBuffer = function(gl, segments, rings, positionAttributeLocation)
{
    this.vertices = [];
    this.vertices.length = (2 + rings * segments) * 3;
    this.vertexCount = 0;
    var deltaTheta = 2 * Math.PI / segments;

    // Create top vertex:
    var index = this.vertexCount * 3;
    this.vertices[index] = 0.0;
    this.vertices[index + 1] = 0.0;
    this.vertices[index + 2] = 1.0;
    this.vertexCount++;

    var deltaRadius = 1.0 / rings;
    var deltaZ = 2.0 / rings;
    for(var i = 1; i < rings; i++)
    {
        var radius = deltaRadius * i;
        var z = 1.0 - deltaZ * i;
        this.createBase(segments, deltaTheta, radius, z);
    }

    // Create bottom circle:
    this.createBase(segments, deltaTheta, 1.0, -1.0);
    index = this.vertexCount * 3;
    this.vertices[index] = 0.0;
    this.vertices[index + 1] = 0.0;
    this.vertices[index + 2] = -1.0;
    this.vertexCount++;

    // Create vertex buffer:
    this.vertexBuffer = new GLBuffer();
    this.vertexBuffer.initialize(gl, gl.ARRAY_BUFFER, flatten(this.vertices), this.vertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 3, positionAttributeLocation);
};

Cone.prototype.initializeColorBuffer = function(gl, colorAttributeLocation)
{
    this.colors = [];
    this.colors.length = this.vertexCount * 4;

    for(var i = 0; i < this.vertexCount; i++)
    {
        var vertexIndex = i * 3;
        var colorIndex = i * 4;

        this.colors[colorIndex] = (this.vertices[vertexIndex] + 1.0) / 2.0;
        this.colors[colorIndex + 1] = (this.vertices[vertexIndex + 1] + 1.0) / 2.0;
        this.colors[colorIndex + 2] = (this.vertices[vertexIndex + 2] + 1.0) / 2.0;
        this.colors[colorIndex + 3] = 1.0;
    }

    // Create color buffer:
    this.colorBuffer = new GLBuffer();
    this.colorBuffer.initialize(gl, gl.ARRAY_BUFFER, flatten(this.colors), this.vertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 4, colorAttributeLocation);
};

Cone.prototype.initializeIndexBuffer = function(gl, segments, rings)
{
    this.indices = [];
    this.indices.length = (2 * segments * rings) * 3;
    var index = 0;

    for(var i = 0; i < segments; i++)
    {
        var i1 = 0;
        var i2 = i + 1;
        var i3 = 1 + i2 % segments;

        this.indices[index++] = i1;
        this.indices[index++] = i2;
        this.indices[index++] = i3;
    }

    for(var r = 0; r < rings - 1; r++)
    {
        for(var s = 0; s < segments; s++)
        {
            var offset = 1 + r * segments;
            var i1 = s + offset;
            var i2 = i1 % segments + offset;
            var i3 = i1 + segments;
            var i4 = i2 + segments;

            this.indices[index++] = i1;
            this.indices[index++] = i3;
            this.indices[index++] = i4;

            this.indices[index++] = i1;
            this.indices[index++] = i4;
            this.indices[index++] = i2;
        }
    }

    for(var i = 0; i < segments; i++)
    {
        var offset = (rings - 1) * segments;
        var i1 = this.vertexCount - 1;
        var i2 = i + 1 + offset;
        var i3 = 1 + i2 % segments + offset;

        this.indices[index++] = i1;
        this.indices[index++] = i3;
        this.indices[index++] = i2;
    }

    // Create index buffer:
    this.indexBuffer = new GLIndexBuffer();
    this.indexBuffer.initialize(gl, new Uint16Array(this.indices));
};

Cone.prototype.createBase = function(segments, deltaTheta, radius, z)
{
    for(var i = 0; i < segments; i++)
    {
        var theta = i * deltaTheta;
        var index = this.vertexCount * 3;

        this.vertices[index] = radius * Math.cos(theta);
        this.vertices[index + 1] = radius * Math.sin(theta);
        this.vertices[index + 2] = z;
        this.vertexCount++;
    }
};
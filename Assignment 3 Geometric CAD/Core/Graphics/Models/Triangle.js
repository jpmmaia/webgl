function Triangle()
{
    this.super = new AbstractModel();
}

Triangle.prototype.initialize = function(gl, positionAttribLocation, colorAttribLocation)
{
    var radius = 0.5;
    var angle1 = Math.PI / 2.0;
    var angle2 = 7.0 * Math.PI / 6.0;
    var angle3 = 11.0 * Math.PI / 6.0;

    this.vertices =
        [
            radius * Math.cos(angle1), radius * Math.sin(angle1), 0.0,
            radius * Math.cos(angle2), radius * Math.sin(angle2), 0.0,
            radius * Math.cos(angle3), radius * Math.sin(angle3), 0.0
        ];

    this.colors =
        [
            1.0, 0.0, 0.0, 1.0,
            0.0, 1.0, 0.0, 1.0,
            0.0, 0.0, 1.0, 1.0
        ];

    this.indices =
        [
            0, 1, 2
        ];

    this.super.initialize(gl, positionAttribLocation, colorAttribLocation, flatten(this.vertices), flatten(this.colors));

    this.indexBuffer = new GLIndexBuffer();
    this.indexBuffer.initialize(gl, new Uint16Array(this.indices));
};

Triangle.prototype.shutdown = function(gl)
{
    this.super.shutdown(gl);
};

Triangle.prototype.render = function(gl)
{
    this.super.render(gl);

    this.indexBuffer.draw(gl);
};
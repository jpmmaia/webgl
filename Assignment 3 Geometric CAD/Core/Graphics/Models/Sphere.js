function Sphere()
{
}

Sphere.prototype.initialize = function(gl, segments, rings, positionAttributeLocation, colorAttributeLocation)
{
    var deltaPhi = 2 * Math.PI / segments;
    var deltaTheta = Math.PI / rings;

    this.vertexCount = 0;
    var maxVertexCount = segments * rings + 1;
    this.vertices = [];
    this.vertices.length = maxVertexCount * 3;
    this.colors = [];
    this.colors.length = maxVertexCount * 4;

    // Add top vertex:
    this.vertices[0] = 0.0;
    this.vertices[1] = 0.0;
    this.vertices[2] = 1.0;
    this.colors[0] = 0.0;
    this.colors[1] = 0.0;
    this.colors[2] = 1.0;
    this.colors[3] = 1.0;
    this.vertexCount++;

    // Add middle vertices:
    for(var i = 0; i < segments; i++)
    {
        var phi = i * deltaPhi;
        var sinPhi = Math.sin(phi);
        var cosPhi = Math.cos(phi);

        for(var j = 1; j <= rings; j++)
        {
            var theta = j * deltaTheta;
            var sinTheta = Math.sin(theta);
            var cosTheta = Math.cos(theta);

            var vertexIndex = this.vertexCount * 3;
            this.vertices[vertexIndex] = sinTheta * cosPhi;
            this.vertices[vertexIndex + 1] = sinTheta * sinPhi;
            this.vertices[vertexIndex + 2] = cosTheta;

            var colorIndex = this.vertexCount * 4;
            this.colors[colorIndex] = (this.vertices[vertexIndex] + 1.0) / 2.0;
            this.colors[colorIndex + 1] = (this.vertices[vertexIndex + 1] + 1.0) / 2.0;
            this.colors[colorIndex + 2] = (this.vertices[vertexIndex + 2] + 1.0) / 2.0;
            this.colors[colorIndex + 3] = 1.0;

            this.vertexCount++;
        }
    }

    // Initialize vertex buffer:
    this.vertexBuffer = new GLBuffer();
    this.vertexBuffer.initialize(gl, gl.ARRAY_BUFFER, flatten(this.vertices), this.vertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 3, positionAttributeLocation);

    // Initialize color buffer:
    this.colorBuffer = new GLBuffer();
    this.colorBuffer.initialize(gl, gl.ARRAY_BUFFER, flatten(this.colors), this.vertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 4, colorAttributeLocation);

    // Initialize index buffer:
    this.initializeIndexBuffer(gl, segments, rings);
};

Sphere.prototype.shutdown = function(gl)
{
    this.colorBuffer.shutdown(gl);
    this.vertexBuffer.shutdown(gl);
};

Sphere.prototype.render = function(gl)
{
    this.vertexBuffer.bind(gl);
    this.colorBuffer.bind(gl);

    this.indexBuffer.draw(gl, gl.TRIANGLES);
};

Sphere.prototype.initializeIndexBuffer = function(gl, segments, rings)
{
    var triangleCount = segments * (1 + 2 * (rings - 1));

    this.indices = [];
    this.indices.length = triangleCount * 3;

    var index = 0;
    for(var i = 0; i < segments; i++)
    {
        this.indices[index++] = -1;
        this.indices[index++] = ((i + 1) * rings) % (rings * segments);
        this.indices[index++] = i * rings;
    }

    for(var i = 0; i < segments; i++)
    {
        this.indices[index++] = i * rings;
        this.indices[index++] = ((i + 1) * rings) % (rings * segments);
        this.indices[index++] = i * rings + 1;

        this.indices[index++] = i * rings + 1;
        this.indices[index++] = ((i + 1) * rings) % (rings * segments);
        this.indices[index++] = ((i + 1) * rings + 1) % (rings * segments);
    }

    for (var i = 0; i < 2 * segments * (rings - 2) * 3; i++)
       this.indices[9 * segments + i] = this.indices[3 * segments + i] + 1;

    for (var i = 0; i < this.indices.length; i++)
        this.indices[i] = this.indices[i] + 1;

    // Initialize index buffer:
    this.indexBuffer = new GLIndexBuffer();
    this.indexBuffer.initialize(gl, new Uint16Array(this.indices));
};
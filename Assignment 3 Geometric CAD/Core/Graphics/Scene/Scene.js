function Scene()
{
}

Scene.prototype.initialize = function(gl, shaderManager)
{
    // Set clear color:
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // Create scene graph:
    this.sceneGraph = new SceneGraph();
    this.sceneGraph.initialize();

    // Set color shader:
    shaderManager.setColorShader(gl);
    this.objectCount = 0;

    // Create hash tables to hold scene objects:
    this.cameras = [];
    this.models = [];
    this.transformComponents = [];
};

Scene.prototype.shutdown = function(gl)
{
    for(var i = 0; i < this.models.length; i++)
        this.models[i].shutdown(gl);

    this.models.length = 0;
};

Scene.prototype.update = function()
{
    this.sceneGraph.update();
};

Scene.prototype.render = function(gl, shaderManager)
{
    // Set color shader:
    shaderManager.setColorShader(gl);

    // Set view matrix:
    shaderManager.setViewMatrix(gl, flatten(this.currentCamera.getViewMatrix()));

    // Render all models:
    for(var i = 0; i < this.models.length; i++)
    {
        var model = this.models[i];
        if(model === null)
            continue;

        var modelTransformComponent = this.transformComponents[i];
        shaderManager.setModelMatrix(gl, flatten(modelTransformComponent.getTransform()));

        model.render(gl);
    }
};

Scene.prototype.addCamera = function(position)
{
    // Create an ID:
    var id = this.objectCount++;

    // Create a camera component:
    var cameraComponent = new CameraComponent();
    cameraComponent.initialize(id);

    // Set position:
    cameraComponent.translate(position[0], position[1], position[2]);

    // Add camera to hash table:
    this.cameras[id] = cameraComponent;

    // Add transform corresponding to the camera:
    this.addTransform(id, "Root");

    return id;
};

Scene.prototype.addSphere = function(gl, shaderManager)
{
    // Create an ID:
    var id = this.objectCount++;

    // Create sphere model:
    var sphereModel = new Sphere();
    sphereModel.initialize(gl, 32, 16, shaderManager.colorShader.position, shaderManager.colorShader.color);
    this.addModel(id, sphereModel);

    // Add transform corresponding to the model:
    this.addTransform(id, "Root");

    return id;
};

Scene.prototype.addCylinder = function(gl, shaderManager)
{
    // Create an ID:
    var id = this.objectCount++;

    // Create cylinder model:
    var cylinderModel = new Cylinder();
    cylinderModel.initialize(gl, 32, 2, shaderManager.colorShader.position, shaderManager.colorShader.color);
    this.addModel(id, cylinderModel);

    // Add transform corresponding to the model:
    this.addTransform(id, "Root");

    return id;
};

Scene.prototype.addCone = function(gl, shaderManager)
{
    // Create an ID:
    var id = this.objectCount++;

    // Create cone model:
    var coneModel = new Cone();
    coneModel.initialize(gl, 32, 1, shaderManager.colorShader.position, shaderManager.colorShader.color);
    this.addModel(id, coneModel);

    // Add transform corresponding to the model:
    this.addTransform(id, "Root");

    return id;
};

Scene.prototype.addModel = function(id, model)
{
    if(id > this.models.length)
    {
        for(var i = this.models.length; i < id; i++)
            this.models[i] = null;
    }

    this.models[id] = model;
};

Scene.prototype.removeModel = function(id, gl)
{
    var model = this.models[id];
    this.models[id] = null;

    // Shutdown model:
    model.shutdown(gl);

    this.removeTransform(id);
};

Scene.prototype.addTransform = function(id, parentId)
{
    // Create transform component:
    var transformComponent = new TransformComponent();
    transformComponent.initialize(id);
    this.transformComponents[id] = transformComponent;

    // Add transform to scene graph:
    this.sceneGraph.addNode(transformComponent, parentId);
};

Scene.prototype.removeTransform = function(id)
{
    this.sceneGraph.removeNode(id);
    this.transformComponents[id] = null;
};

Scene.prototype.setActiveCamera = function(id, gl, shaderManager)
{
    this.currentCamera = this.cameras[id];

    // Setup projection matrix:
    shaderManager.setProjectionMatrix(gl, flatten(this.currentCamera.getProjectionMatrix()));
};
Scene.prototype.setOrthogonalMode = function(gl, shaderManager, aspectRatio)
{
    this.currentCamera.setOrthogonal(-3.0 * aspectRatio, 3.0 * aspectRatio, -3.0, 3.0, -20.0, 20.0);
    shaderManager.setProjectionMatrix(gl, flatten(this.currentCamera.getProjectionMatrix()));

    this.render(gl, shaderManager);
};
Scene.prototype.setPerspectiveMode = function(gl, shaderManager, fieldOfViewY, aspectRatio)
{
    this.currentCamera.setPerspective(fieldOfViewY, aspectRatio, 0.1, 20.0);
    shaderManager.setProjectionMatrix(gl, flatten(this.currentCamera.getProjectionMatrix()));
};

Scene.prototype.getTransform = function(id)
{
    return this.transformComponents[id];
};
function Entity()
{
}

Entity.prototype.initialize = function(id)
{
    this.id = id;
    this.cameraComponent = null;
    this.model = null;

    // Initialize transform component:
    this.transformComponent = new TransformComponent();
    this.transformComponent.initialize(this.id);
};

Entity.prototype.getId = function()
{
    return this.id;
};

Entity.prototype.getCameraComponent = function()
{
    return this.cameraComponent;
};

Entity.prototype.setCameraComponent = function(value)
{
    this.cameraComponent = value;
};

Entity.prototype.getModel = function()
{
    return this.model;
};

Entity.prototype.setModel = function(value)
{
    this.model = value;
};

Entity.prototype.getTransformComponent = function()
{
    return this.transformComponent;
};
function TransformComponent()
{
}

TransformComponent.prototype.initialize = function(id)
{
    this.id = id;
    this.transform = mat4(1);
};

TransformComponent.prototype.getId = function()
{
    return this.id;
};

TransformComponent.prototype.getTransform = function()
{
    return this.transform;
};

TransformComponent.prototype.setTransform = function(value)
{
    this.transform = value;
};
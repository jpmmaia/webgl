function ColorShader()
{
    this.super = new AbstractShader();
}

ColorShader.prototype.initialize = function(gl)
{
    this.super.initialize(gl, "ColorVertexShader", "ColorFragmentShader");

    // Get uniforms locations:
    this.modelMatrix = gl.getUniformLocation(this.super.program, "uModelMatrix");
    this.viewMatrix = gl.getUniformLocation(this.super.program, "uViewMatrix");
    this.projectionMatrix = gl.getUniformLocation(this.super.program, "uProjectionMatrix");

    // Get attributes locations:
    this.position = gl.getAttribLocation(this.super.program, "vPosition");
    this.color = gl.getAttribLocation(this.super.program, "vColor");
};

ColorShader.prototype.setShader = function(gl)
{
    this.super.setShader(gl);
};

ColorShader.prototype.setModelMatrix = function(gl, value)
{
    gl.uniformMatrix4fv(this.modelMatrix, false, value);
};

ColorShader.prototype.setViewMatrix = function(gl, value)
{
    gl.uniformMatrix4fv(this.viewMatrix, false, value);
};

ColorShader.prototype.setProjectionMatrix = function(gl, value)
{
    gl.uniformMatrix4fv(this.projectionMatrix, false, value);
};
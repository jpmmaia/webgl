function ShaderManager()
{
}

ShaderManager.prototype.initialize = function(gl)
{
    // Initialize color shader:
    this.colorShader = new ColorShader();
    this.colorShader.initialize(gl);
};

ShaderManager.prototype.setColorShader = function(gl)
{
    this.colorShader.setShader(gl);

    this.currentShader = this.colorShader;
};

ShaderManager.prototype.setModelMatrix = function(gl, value)
{
    this.currentShader.setModelMatrix(gl, value);
};

ShaderManager.prototype.setViewMatrix = function(gl, value)
{
    this.currentShader.setViewMatrix(gl, value);
};

ShaderManager.prototype.setProjectionMatrix = function(gl, value)
{
    this.currentShader.setProjectionMatrix(gl, value);
};
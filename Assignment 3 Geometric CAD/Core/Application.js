function Application()
{
}

Application.prototype.initialize = function(canvas)
{
    // Set canvas:
    this.canvas = canvas;
    this.aspectRatio = this.canvas.width / this.canvas.height;
    this.fieldOfViewY = 30.0;

    // Create graphics object:
    this.graphics = new Graphics();
    this.graphics.initialize(canvas);

    this.sceneObjects = {};
    this.matrices = {};
    this.coneCount = 0;
    this.cylinderCount = 0;
    this.sphereCount = 0;
    this.currentObject = null;

    // Create a scene:
    this.scene = new Scene();
    this.scene.initialize(this.graphics.gl, this.graphics.shaderManager);

    // Setup a default camera:
    var cameraId = this.scene.addCamera([0.0, 0.0, 10.0]);
    this.scene.setActiveCamera(cameraId, this.graphics.gl, this.graphics.shaderManager);
    this.scene.setPerspectiveMode(this.graphics.gl, this.graphics.shaderManager, this.fieldOfViewY, this.aspectRatio);

    // Render scene:
    this.render();
};

Application.prototype.render = function()
{
    this.graphics.beginScene();
    this.scene.render(this.graphics.gl, this.graphics.shaderManager);
};

Application.prototype.translateCurrentObject = function(index, value)
{
    if(this.currentObject === null)
        return;

    var matrix = this.matrices[this.currentObject];
    matrix[0][index] = value;

    this.transformCurrentObject();
};

Application.prototype.rotateCurrentObject = function(axis, value)
{
    if(this.currentObject === null)
        return;

    var matrix = this.matrices[this.currentObject];
    matrix[1][axis] = value;

    this.transformCurrentObject();
};

Application.prototype.scaleCurrentObject = function(index, value)
{
    if(this.currentObject === null)
        return;

    var matrix = this.matrices[this.currentObject];
    matrix[2][index] = value;

    this.transformCurrentObject();
};

Application.prototype.transformCurrentObject = function()
{
    if(this.currentObject === null)
        return;

    var matrix = this.matrices[this.currentObject];

    var translateMatrix1 = translate(matrix[0][0], matrix[0][1], matrix[0][2]);
    var rotateXMatrix = rotate(matrix[1][0], [1, 0, 0]);
    var rotateYMatrix = rotate(matrix[1][1], [0, 1, 0]);
    var rotateZMatrix = rotate(matrix[1][2], [0, 0, 1]);
    var scaleMatrix = scalem(matrix[2][0], matrix[2][1], matrix[2][2]);

    var transform = mult(translateMatrix1, rotateXMatrix);
    transform = mult(transform, rotateYMatrix);
    transform = mult(transform, rotateZMatrix);
    transform = mult(transform, scaleMatrix);

    var objectId = this.sceneObjects[this.currentObject];
    var objectTransformComponent = this.scene.getTransform(objectId);
    objectTransformComponent.setTransform(transform);

    this.render();
};

Application.prototype.addCone = function(position, rotation)
{
    var id = "Cone" + this.coneCount++;

    var sceneId = this.scene.addCone(this.graphics.gl, this.graphics.shaderManager);
    this.addSceneObject(id, sceneId, position, rotation);

    return id;
};

Application.prototype.addCylinder = function(position, rotation)
{
    var id = "Cylinder" + this.cylinderCount++;

    var sceneId = this.scene.addCylinder(this.graphics.gl, this.graphics.shaderManager);
    this.addSceneObject(id, sceneId, position, rotation);

    return id;
};

Application.prototype.addSphere = function(position, rotation)
{
    var id = "Sphere" + this.sphereCount++;

    var sceneId = this.scene.addSphere(this.graphics.gl, this.graphics.shaderManager);
    this.addSceneObject(id, sceneId, position, rotation);

    return id;
};

Application.prototype.addSceneObject = function(id, sceneId, position, rotation)
{
    var matrix =
        [
            [position[0], position[1], position[2]],
            [rotation[0], rotation[1], rotation[2]],
            [1.0, 1.0, 1.0]
        ];

    this.matrices[id] = matrix;
    this.sceneObjects[id] = sceneId;
    this.currentObject = id;

    this.transformCurrentObject();
};

Application.prototype.removeSceneObject = function(id)
{
    if(this.currentObject === null || id === "")
        return false;

    if(this.currentObject === id)
        this.currentObject = null;

    this.matrices[id] = null;
    var sceneId = this.sceneObjects[id];
    this.scene.removeModel(sceneId, this.graphics.gl);
    this.sceneObjects[id] = null;

    this.render();

    return true;
};

Application.prototype.getCurrentObjectMatrix = function()
{
    return this.matrices[this.currentObject];
};

Application.prototype.setCurrentObject = function(id)
{
    this.currentObject = id;
};

Application.prototype.setOrthogonalMode = function()
{
    this.scene.setOrthogonalMode(this.graphics.gl, this.graphics.shaderManager, this.aspectRatio);
    this.render();
};
Application.prototype.setPerspectiveMode = function()
{
    this.scene.setPerspectiveMode(this.graphics.gl, this.graphics.shaderManager, this.fieldOfViewY, this.aspectRatio);
    this.render();
};
var application;

window.onload = function initialize()
{
    // Get canvas element:
    var canvas = document.getElementById("gl-canvas");

    // Initialize application:
    application = new Application();
    application.initialize(canvas);

    // Initialize user interface:
    initializeUserInterface(canvas);

    onObjectChange();
};

function initializeUserInterface(canvas)
{
    var createObjectInput = document.getElementById("CreateObject");
    createObjectInput.onclick = onCreateObjectClick;

    var objectInput = document.getElementById("Object");
    objectInput.onchange = onObjectChange;

    var deleteObjectInput = document.getElementById("DeleteObject");
    deleteObjectInput.onclick = onDeleteObjectClick;

    var translateXInput = document.getElementById("TranslateX");
    translateXInput.onchange = onTranslateXChange;
    var translateYInput = document.getElementById("TranslateY");
    translateYInput.onchange = onTranslateYChange;
    var translateZInput = document.getElementById("TranslateZ");
    translateZInput.onchange = onTranslateZChange;

    var rotateXInput = document.getElementById("RotateX");
    rotateXInput.onchange = onRotateXChange;
    var rotateYInput = document.getElementById("RotateY");
    rotateYInput.onchange = onRotateYChange;
    var rotateZInput = document.getElementById("RotateZ");
    rotateZInput.onchange = onRotateZChange;

    var scaleXInput = document.getElementById("ScaleX");
    scaleXInput.onchange = onScaleXChange;
    var scaleYInput = document.getElementById("ScaleY");
    scaleYInput.onchange = onScaleYChange;
    var scaleZInput = document.getElementById("ScaleZ");
    scaleZInput.onchange = onScaleZChange;

    var orthogonalModeInput = document.getElementById("OrthogonalMode");
    orthogonalModeInput.onclick = onOrthogonalModeClick;
    var perspectiveModeInput = document.getElementById("PerspectiveMode");
    perspectiveModeInput.onclick = onPerspectiveModeClick;
}

function onCreateObjectClick()
{
    var primitiveTypeInput = document.getElementById("PrimitiveType");
    var value = primitiveTypeInput.value;

    var id;
    if(value === "Cone")
        id = application.addCone([0.0, 0.0, -2.0], [-90.0, 0.0, 0.0]);
    else if(value === "Cylinder")
        id = application.addCylinder([0.0, 0.0, -2.0], [-90.0, 0.0, 0.0]);
    else if(value === "Sphere")
        id = application.addSphere([0.0, 0.0, -2.0], [-90.0, 0.0, 0.0]);

    var option = document.createElement("option");
    option.text = id;
    option.value = id;

    var objectInput = document.getElementById("Object");
    objectInput.options.add(option);
    objectInput.value = id;

    onObjectChange();
}

function onObjectChange()
{
    var objectInput = document.getElementById("Object");
    var value = objectInput.value;

    if(value === "")
        return;

    application.setCurrentObject(value);

    // Update interface values:
    var matrix = application.getCurrentObjectMatrix();

    var translateXInput = document.getElementById("TranslateX");
    translateXInput.value = matrix[0][0];

    var translateYInput = document.getElementById("TranslateY");
    translateYInput.value = matrix[0][1];

    var translateZInput = document.getElementById("TranslateZ");
    translateZInput.value = matrix[0][2];

    var rotateXInput = document.getElementById("RotateX");
    rotateXInput.value = matrix[1][0];

    var rotateYInput = document.getElementById("RotateY");
    rotateYInput.value = matrix[1][1];

    var rotateZInput = document.getElementById("RotateZ");
    rotateZInput.value = matrix[1][2];

    var scaleXInput = document.getElementById("ScaleX");
    scaleXInput.value = matrix[2][0];

    var scaleYInput = document.getElementById("ScaleY");
    scaleYInput.value = matrix[2][1];

    var scaleZInput = document.getElementById("ScaleZ");
    scaleZInput.value = matrix[2][2];
}

function onDeleteObjectClick()
{
    var objectInput = document.getElementById("Object");
    var value = objectInput.value;

    if(application.removeSceneObject(value) === true)
    {
        objectInput.options.remove(objectInput.selectedIndex);

        value = objectInput.value;
        if(value === "")
            value = null;

        application.setCurrentObject(value);
        onObjectChange();
    }
}

function onTranslateXChange()
{
    var translateXInput = document.getElementById("TranslateX");
    var value = translateXInput.value;
    application.translateCurrentObject(0, value);
}
function onTranslateYChange()
{
    var translateYInput = document.getElementById("TranslateY");
    var value = translateYInput.value;
    application.translateCurrentObject(1, value);
}
function onTranslateZChange()
{
    var translateZInput = document.getElementById("TranslateZ");
    var value = translateZInput.value;
    application.translateCurrentObject(2, value);
}

function onRotateXChange()
{
    var element = document.getElementById("RotateX");
    var value = element.value;
    application.rotateCurrentObject(0, value);
}
function onRotateYChange()
{
    var element = document.getElementById("RotateY");
    var value = element.value;
    application.rotateCurrentObject(1, value);
}
function onRotateZChange()
{
    var element = document.getElementById("RotateZ");
    var value = element.value;
    application.rotateCurrentObject(2, value);
}

function onScaleXChange()
{
    var element = document.getElementById("ScaleX");
    var value = element.value;
    application.scaleCurrentObject(0, value);
}
function onScaleYChange()
{
    var element = document.getElementById("ScaleY");
    var value = element.value;
    application.scaleCurrentObject(1, value);
}
function onScaleZChange()
{
    var element = document.getElementById("ScaleZ");
    var value = element.value;
    application.scaleCurrentObject(2, value);
}

function onOrthogonalModeClick()
{
    application.setOrthogonalMode();
}
function onPerspectiveModeClick()
{
    application.setPerspectiveMode();
}
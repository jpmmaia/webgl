var application;

window.onload = function initialize()
{
    // Get canvas element:
    var canvas = document.getElementById("gl-canvas");

    // Initialize application:
    application = new Application();
    application.initialize(canvas);

    // Initialize user interface:
    initializePrimitiveForm();
    //initializeLightForm();
    //initializeOtherForm();

    onObjectChange();
    //onLightChange();

    updateRotationValues();
    run();
};

function run()
{
    application.render();

    setTimeout(
        function()
        {
            requestAnimFrame(run);
        },
        30
    );
}

// <------------- INITIALIZE PRIMITIVE FORM ------------->

function initializePrimitiveForm()
{
    var objectInput = document.getElementById("Object");

    objectInput.onchange = onObjectChange;

    document.getElementById("RotateX").oninput = onRotateXInput;
    document.getElementById("RotateY").oninput = onRotateYInput;
    document.getElementById("RotateZ").oninput = onRotateZInput;

    addObjectOption("SphericalCheckerboardSphere", "Checkerboard (spherical texture mapping + regular pattern)");
    addObjectOption("PlanarCheckerboardSphere", "Checkerboard (planar texture mapping + regular pattern)");
    addObjectOption("SphericalGlobeSphere", "Globe (spherical texture mapping + standard image)");
    addObjectOption("PlanarGlobeSphere", "Globe (planar texture mapping + standard image)");
    addObjectOption("GlobeDoubleTextureModel", "Checkeboard + Globe Noise (regular pattern + standard image + spherical texture mapping");
    addObjectOption("SphericalNoiseGlobeSphere", "Perlin Noise (spherical texture mapping + generated image)");
    addObjectOption("SphericalFireAndIceSphere", "Fire and Ice (spherical texture mapping + standard image");
    addObjectOption("FireDoubleTextureModel", "Fire and Ice + Perlin Noise (standard image + generated image + spherical texture mapping");
}

function addObjectOption(value, text)
{
    var option = document.createElement("option");
    option.value = value;
    option.text = text;

    var objectInput = document.getElementById("Object");
    objectInput.options.add(option);
}

function onObjectChange()
{
    var objectInput = document.getElementById("Object");
    var value = objectInput.value;

    if(value === "")
        return;

    application.setCurrentObject(value);
}

function updateRotationValues()
{
    var matrix = application.getInterfaceMatrix();

    document.getElementById("RotateX").value = matrix[1][0];
    document.getElementById("RotateY").value = matrix[1][1];
    document.getElementById("RotateZ").value = matrix[1][2];
}
function onRotateXInput()
{
    var element = document.getElementById("RotateX");
    var value = parseInt(element.value);
    application.rotateCurrentObject(0, value);
}
function onRotateYInput()
{
    var element = document.getElementById("RotateY");
    var value = parseInt(element.value);
    application.rotateCurrentObject(1, value);
}
function onRotateZInput()
{
    var element = document.getElementById("RotateZ");
    var value = parseInt(element.value);
    application.rotateCurrentObject(2, value);
}


// <------------- INITIALIZE LIGHT FORM ------------->

function initializeLightForm()
{
    var createLightInput = document.getElementById("CreatePointLight");
    createLightInput.onclick = onCreatePointLightClick;

    var lightInput = document.getElementById("Light");
    lightInput.onchange = onLightChange;

    var deleteLightInput = document.getElementById("DeleteLight");
    deleteLightInput.onclick = onDeletetLightClick;

    var activeLightInput = document.getElementById("ActiveLight");
    activeLightInput.onchange = onActivetLightChange;

    var translateLightXInput = document.getElementById("TranslateLightX");
    translateLightXInput.oninput = onTranslateLightXInput;
    var translateLightYInput = document.getElementById("TranslateLightY");
    translateLightYInput.oninput = onTranslateLightYInput;
    var translateLightZInput = document.getElementById("TranslateLightZ");
    translateLightZInput.oninput = onTranslateLightZInput;

    var lightDiffuseColorInput = document.getElementById("LightDiffuseColor");
    lightDiffuseColorInput.oninput = onLightDiffuseColorInput;
    var lightSpecularColorInput = document.getElementById("LightSpecularColor");
    lightSpecularColorInput.oninput = onLightSpecularColorInput;
    var lightAmbientColorInput = document.getElementById("LightAmbientColor");
    lightAmbientColorInput.oninput = onLightAmbientColorInput;

    document.getElementById("LightLinearAttenuation").oninput = onLightLinearAttenuationInput;
    document.getElementById("LightSquareAttenuation").oninput = onLightSquareAttenuationInput;

    document.getElementById("AnimateLights").onchange = onAnimateLightsChange;
}

function onCreatePointLightClick()
{
    var position = [];
    position[0] = parseFloat(document.getElementById("TranslateLightX").value);
    position[1] = parseFloat(document.getElementById("TranslateLightY").value);
    position[2] = parseFloat(document.getElementById("TranslateLightZ").value);

    var diffuseColor = hexToColor(document.getElementById("LightDiffuseColor").value);
    var specularColor = hexToColor(document.getElementById("LightSpecularColor").value);
    var ambientColor = hexToColor(document.getElementById("LightAmbientColor").value);

    createLight(position, diffuseColor, specularColor, ambientColor);
}
function createLight(position, diffuseColor, specularColor, ambientColor)
{
    var id = application.addLight(position, diffuseColor, specularColor, ambientColor);

    var option = document.createElement("option");
    option.text = id;
    option.value = id;

    var lightInput = document.getElementById("Light");
    lightInput.options.add(option);
    lightInput.value = id;
}

function onLightChange()
{
    var lightInput = document.getElementById("Light");
    var value = lightInput.value;
    if(value === "")
        return;

    application.setCurrentLight(value);

    // Update interface values:
    var light = application.getCurrentLight();

    var position = light.getPosition();
    document.getElementById("TranslateLightX").value = position[0];
    document.getElementById("TranslateLightY").value = position[1];
    document.getElementById("TranslateLightZ").value = position[2];

    document.getElementById("LightDiffuseColor").value = colorToHex(light.getDiffuseColor());
    document.getElementById("LightSpecularColor").value = colorToHex(light.getSpecularColor());
    document.getElementById("LightAmbientColor").value = colorToHex(light.getAmbientColor());
    document.getElementById("ActiveLight").checked = light.isActive();
}

function onDeletetLightClick()
{
    if(application.removeCurrentLight())
    {
        var element = document.getElementById("Light");
        element.options.remove(element.selectedIndex);

        value = element.value;
        if(value === "")
            value = null;

        application.setCurrentLight(value);

        onLightChange();
    }
}

function onActivetLightChange()
{
    application.setCurrentLightActive(document.getElementById("ActiveLight").checked);
}

function onTranslateLightXInput()
{
    var light = application.getCurrentLight();
    var position = light.getPosition();
    position[0] = document.getElementById("TranslateLightX").value;

    application.setCurrentLightPosition(position);
}
function onTranslateLightYInput()
{
    var light = application.getCurrentLight();
    var position = light.getPosition();
    position[1] = document.getElementById("TranslateLightY").value;

    application.setCurrentLightPosition(position);
}
function onTranslateLightZInput()
{
    var light = application.getCurrentLight();
    var position = light.getPosition();
    position[2] = document.getElementById("TranslateLightZ").value;

    application.setCurrentLightPosition(position);
}

function onLightDiffuseColorInput()
{
    var color = hexToColor(document.getElementById("LightDiffuseColor").value);
    application.setCurrentLightDiffuseColor(color);
}
function onLightSpecularColorInput()
{
    var color = hexToColor(document.getElementById("LightSpecularColor").value);
    application.setCurrentLightSpecularColor(color);
}
function onLightAmbientColorInput()
{
    var color = hexToColor(document.getElementById("LightAmbientColor").value);
    application.setCurrentLightAmbientColor(color);
}

function onLightLinearAttenuationInput()
{
    application.setLightLinearAttenuation(document.getElementById("LightLinearAttenuation").value);
}

function onLightSquareAttenuationInput()
{
    application.setLightSquareAttenuation(document.getElementById("LightSquareAttenuation").value);
}

// <------------- INITIALIZE OTHER FORM ------------->

function initializeOtherForm()
{
    var orthogonalModeInput = document.getElementById("OrthogonalMode");
    orthogonalModeInput.onclick = onOrthogonalModeClick;
    var perspectiveModeInput = document.getElementById("PerspectiveMode");
    perspectiveModeInput.onclick = onPerspectiveModeClick;

    var globalAmbientColorInput = document.getElementById("GlobalAmbientColor");
    globalAmbientColorInput.oninput = onGlobalAmbientColorInput;
}

function onOrthogonalModeClick()
{
    application.setOrthogonalMode();
}
function onPerspectiveModeClick()
{
    application.setPerspectiveMode();
}

function onGlobalAmbientColorInput()
{
    application.setGlobalAmbientColor(hexToColor(document.getElementById("GlobalAmbientColor").value));
}

function onAnimateLightsChange()
{
    application.setAnimate(document.getElementById("AnimateLights").checked);
}

// <------------- HELPER FUNCTIONS ------------->

function hexToColor(hex)
{
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if(!result)
        return [0.0, 0.0, 0.0];

    return [
        parseInt(result[1], 16) / 255.0,
        parseInt(result[2], 16) / 255.0,
        parseInt(result[3], 16) / 255.0
    ];
}

function colorComponentToHex(component)
{
    var hex = Math.round(component * 255).toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function colorToHex(color)
{
    return "#" + colorComponentToHex(color[0]) + colorComponentToHex(color[1]) + colorComponentToHex(color[2]);
}
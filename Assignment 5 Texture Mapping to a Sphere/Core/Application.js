function Application()
{
}

Application.prototype.initialize = function(canvas)
{
    // Set canvas:
    this.canvas = canvas;
    this.aspectRatio = this.canvas.width / this.canvas.height;
    this.fieldOfViewY = 30.0;

    // Create graphics object:
    this.graphics = new Graphics();
    this.graphics.initialize(canvas);

    this.lights = {};
    this.lightCount = 0;
    this.currentLight = null;
    this.matrix = [
        [0.0, 0.0, 0.0],
        [-90.0, 0.0, 0.0],
        [1.0, 1.0, 1.0]
    ];

    // Create a scene:
    this.scene = new Scene();
    this.scene.initialize(this.graphics.gl, this.graphics.shaderManager);

    // Setup a default camera:
    var cameraId = this.scene.addCamera([0.0, 0.0, 10.0]);
    this.scene.setActiveCamera(cameraId);
    this.scene.setPerspectiveMode(this.fieldOfViewY, this.aspectRatio);

    this.transformCurrentObject();
};

Application.prototype.render = function()
{
    this.graphics.beginScene();
    this.scene.render(this.graphics.gl, this.graphics.shaderManager);
};

Application.prototype.rotateCurrentObject = function(axis, value)
{
    this.matrix[1][axis] = value;

    this.transformCurrentObject();
};

Application.prototype.transformCurrentObject = function()
{
    var matrix = this.matrix;

    var translateMatrix1 = translate(matrix[0][0], matrix[0][1], matrix[0][2]);
    var rotateXMatrix = rotate(matrix[1][0], [1, 0, 0]);
    var rotateYMatrix = rotate(matrix[1][1], [0, 1, 0]);
    var rotateZMatrix = rotate(matrix[1][2], [0, 0, 1]);
    var scaleMatrix = scalem(matrix[2][0], matrix[2][1], matrix[2][2]);

    var transform = mult(translateMatrix1, rotateXMatrix);
    transform = mult(transform, rotateYMatrix);
    transform = mult(transform, rotateZMatrix);
    transform = mult(transform, scaleMatrix);

    this.scene.setSceneObjectTransform(flatten(transform));
};

Application.prototype.addLight = function(position, diffuseColor, specularColor, ambientColor)
{
    var id = "Light" + this.lightCount++;

    var sceneId = this.scene.addLight(this.graphics.gl, this.graphics.shaderManager, position, diffuseColor, specularColor, ambientColor);
    this.lights[id] = sceneId;
    this.currentLight = id;

    return id;
};

Application.prototype.getCurrentLight = function()
{
    var sceneId = this.lights[this.currentLight];

    return this.scene.getLight(sceneId);
};

Application.prototype.setCurrentLight = function(id)
{
    this.currentLight = id;
};

Application.prototype.setOrthogonalMode = function()
{
    this.scene.setOrthogonalMode(this.graphics.gl, this.graphics.shaderManager, this.aspectRatio);
};
Application.prototype.setPerspectiveMode = function()
{
    this.scene.setPerspectiveMode(this.graphics.gl, this.graphics.shaderManager, this.fieldOfViewY, this.aspectRatio);
};

Application.prototype.setCurrentLightPosition = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightPosition(sceneId, value);
};
Application.prototype.setCurrentLightDiffuseColor = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightDiffuseColor(sceneId, value);
};
Application.prototype.setCurrentLightSpecularColor = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightSpecularColor(sceneId, value);
};
Application.prototype.setCurrentLightAmbientColor = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightAmbientColor(sceneId, value);
};
Application.prototype.removeCurrentLight = function()
{
    if(this.currentLight === null)
        return false;

    var sceneId = this.lights[this.currentLight];
    this.lights[this.currentLight] = null;
    this.currentLight = null;

    this.scene.removeLight(this.graphics.gl, this.graphics.shaderManager, sceneId);

    return true;
};
Application.prototype.setCurrentLightActive = function(value)
{
    var sceneId = this.lights[this.currentLight];

    this.scene.setLightActive(this.graphics.gl, this.graphics.shaderManager, sceneId, value);
};

Application.prototype.setGlobalAmbientColor = function(value)
{
    this.scene.setGlobalAmbientColor(this.graphics.gl, this.graphics.shaderManager, value);
};

Application.prototype.setLightLinearAttenuation = function(value)
{
    this.scene.setLightLinearAttenuation(this.graphics.gl, this.graphics.shaderManager, value);
};

Application.prototype.setLightSquareAttenuation = function(value)
{
    this.scene.setLightSquareAttenuation(this.graphics.gl, this.graphics.shaderManager, value);
};

Application.prototype.getInterfaceMatrix = function()
{
    return this.matrix;
};

Application.prototype.setCurrentObject = function(id)
{
    this.scene.setCurrentObject(id);
};
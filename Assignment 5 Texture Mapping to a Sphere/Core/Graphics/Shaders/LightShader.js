function LightShader()
{
    this.super = new AbstractShader();
}

LightShader.prototype.initialize = function(gl)
{
    this.super.initialize(gl, "LightVertexShader", "LightFragmentShader");

    this.maxPointLights = 4;

    // Get matrix buffer location:
    this.modelMatrix = gl.getUniformLocation(this.super.program, "uMatrixBuffer.modelMatrix");
    this.viewMatrix = gl.getUniformLocation(this.super.program, "uMatrixBuffer.viewMatrix");
    this.projectionMatrix = gl.getUniformLocation(this.super.program, "uMatrixBuffer.projectionMatrix");

    // Get point light uniforms location:
    this.pointLights = [];
    this.pointLights.length = this.maxPointLights;
    for(var i = 0; i < this.maxPointLights; i++)
        this.pointLights[i] = this.createPointLight(gl, i);

    // Get camera position and global ambient color uniform locations:
    this.cameraPosition = gl.getUniformLocation(this.super.program, "uCameraPosition");
    this.globalAmbientColor = gl.getUniformLocation(this.super.program, "uGlobalAmbientColor");

    this.linearAttenuation = gl.getUniformLocation(this.super.program, "uLightLinearAttenuation");
    this.squareAttenuation = gl.getUniformLocation(this.super.program, "uLightSquareAttenuation");

    // Get attributes locations:
    this.position = gl.getAttribLocation(this.super.program, "vPosition");
    this.normal = gl.getAttribLocation(this.super.program, "vNormal");
};

LightShader.prototype.setShader = function(gl)
{
    this.super.setShader(gl);
};

LightShader.prototype.setModelMatrix = function(gl, value)
{
    gl.uniformMatrix4fv(this.modelMatrix, false, value);
};

LightShader.prototype.setViewMatrix = function(gl, value)
{
    gl.uniformMatrix4fv(this.viewMatrix, false, value);
};

LightShader.prototype.setProjectionMatrix = function(gl, value)
{
    gl.uniformMatrix4fv(this.projectionMatrix, false, value);
};

LightShader.prototype.setCameraPosition = function(gl, value)
{
    gl.uniform3fv(this.cameraPosition, value);
};

LightShader.prototype.setGlobalAmbientColor = function(gl, value)
{
    gl.uniform3fv(this.globalAmbientColor, value);
};

LightShader.prototype.setLinearAttenuation = function(gl, value)
{
    gl.uniform1f(this.linearAttenuation, value);
};

LightShader.prototype.setSquareAttenuation = function(gl, value)
{
    gl.uniform1f(this.squareAttenuation, value);
};

LightShader.prototype.setLightPosition = function(gl, index, value)
{
    gl.uniform3fv(this.pointLights[index].position, value);
};

LightShader.prototype.setLightDiffuseColor = function(gl, index, value)
{
    gl.uniform3fv(this.pointLights[index].diffuseColor, value);
};

LightShader.prototype.setLightSpecularColor = function(gl, index, value)
{
    gl.uniform3fv(this.pointLights[index].specularColor, value);
};

LightShader.prototype.setLightAmbientColor = function(gl, index, value)
{
    gl.uniform3fv(this.pointLights[index].ambientColor, value);
};

LightShader.prototype.setLightActive = function(gl, index, value)
{
    gl.uniform1i(this.pointLights[index].active, value);
};

LightShader.prototype.createPointLight = function(gl, index)
{
    var pointLight = {};
    pointLight.position = gl.getUniformLocation(this.super.program, "uLightPositions[" + index + "]");

    var name = "uPointLightParameters[" + index + "].";
    pointLight.diffuseColor = gl.getUniformLocation(this.super.program, name + "diffuseColor");
    pointLight.specularColor = gl.getUniformLocation(this.super.program, name + "specularColor");
    pointLight.ambientColor = gl.getUniformLocation(this.super.program, name + "ambientColor");
    pointLight.active = gl.getUniformLocation(this.super.program, name + "active");

    return pointLight;
};
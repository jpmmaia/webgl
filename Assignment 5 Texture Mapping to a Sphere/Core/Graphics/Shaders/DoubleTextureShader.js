function DoubleTextureShader()
{
    this.super = new AbstractShader();
}

DoubleTextureShader.prototype.initialize = function(gl)
{
    this.super.initialize(gl, "DoubleTextureVertexShader", "DoubleTextureFragmentShader");

    // Get matrix buffer location:
    this.modelMatrix = gl.getUniformLocation(this.super.program, "uMatrixBuffer.modelMatrix");
    this.viewMatrix = gl.getUniformLocation(this.super.program, "uMatrixBuffer.viewMatrix");
    this.projectionMatrix = gl.getUniformLocation(this.super.program, "uMatrixBuffer.projectionMatrix");
    this.texture0 = gl.getUniformLocation(this.super.program, "uTexture0");
    this.texture1 = gl.getUniformLocation(this.super.program, "uTexture1");

    // Get attributes locations:
    this.position = gl.getAttribLocation(this.super.program, "vPosition");
    this.textureCoordinates = gl.getAttribLocation(this.super.program, "vTextureCoordinates");
};

DoubleTextureShader.prototype.setShader = function(gl)
{
    this.super.setShader(gl);
};

DoubleTextureShader.prototype.setModelMatrix = function(gl, value)
{
    gl.uniformMatrix4fv(this.modelMatrix, false, value);
};

DoubleTextureShader.prototype.setViewMatrix = function(gl, value)
{
    gl.uniformMatrix4fv(this.viewMatrix, false, value);
};

DoubleTextureShader.prototype.setProjectionMatrix = function(gl, value)
{
    gl.uniformMatrix4fv(this.projectionMatrix, false, value);
};

DoubleTextureShader.prototype.setTexture0 = function(gl, value)
{
    gl.uniform1i(this.texture0, value);
};

DoubleTextureShader.prototype.setTexture1 = function(gl, value)
{
    gl.uniform1i(this.texture1, value);
};
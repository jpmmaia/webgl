function ShaderManager()
{
}

ShaderManager.prototype.initialize = function(gl)
{
    // Initialize color shader:
    this.colorShader = new ColorShader();
    this.colorShader.initialize(gl);

    // Initialize light shader:
    this.lightShader = new LightShader();
    this.lightShader.initialize(gl);

    // Initialize texture shader:
    this.textureShader = new TextureShader();
    this.textureShader.initialize(gl);

    // Initialize double texture shader:
    this.doubleTextureShader = new DoubleTextureShader();
    this.doubleTextureShader.initialize(gl);
};

ShaderManager.prototype.setColorShader = function(gl)
{
    this.setShader(gl, this.colorShader);
};

ShaderManager.prototype.setLightShader = function(gl)
{
    this.setShader(gl, this.lightShader);
};

ShaderManager.prototype.setTextureShader = function(gl)
{
    this.setShader(gl, this.textureShader);
};

ShaderManager.prototype.setDoubleTextureShader = function(gl)
{
    this.setShader(gl, this.doubleTextureShader);
};

ShaderManager.prototype.setShader = function(gl, shader)
{
    shader.setShader(gl);

    this.currentShader = shader;
};

ShaderManager.prototype.setModelMatrix = function(gl, value)
{
    this.currentShader.setModelMatrix(gl, value);
};

ShaderManager.prototype.setViewMatrix = function(gl, value)
{
    this.currentShader.setViewMatrix(gl, value);
};

ShaderManager.prototype.setProjectionMatrix = function(gl, value)
{
    this.currentShader.setProjectionMatrix(gl, value);
};

ShaderManager.prototype.setCameraPosition = function(gl, value)
{
    this.currentShader.setCameraPosition(gl, value);
};

ShaderManager.prototype.setGlobalAmbientColor = function(gl, value)
{
    this.currentShader.setGlobalAmbientColor(gl, value);
};

ShaderManager.prototype.setLinearAttenuation = function(gl, value)
{
    this.currentShader.setLinearAttenuation(gl, value);
};

ShaderManager.prototype.setSquareAttenuation = function(gl, value)
{
    this.currentShader.setSquareAttenuation(gl, value);
};

ShaderManager.prototype.setLightPosition = function(gl, index, value)
{
    this.currentShader.setLightPosition(gl, index, value);
};

ShaderManager.prototype.setLightDiffuseColor = function(gl, index, value)
{
    this.currentShader.setLightDiffuseColor(gl, index, value);
};

ShaderManager.prototype.setLightSpecularColor = function(gl, index, value)
{
    this.currentShader.setLightSpecularColor(gl, index, value);
};

ShaderManager.prototype.setLightAmbientColor = function(gl, index, value)
{
    this.currentShader.setLightAmbientColor(gl, index, value);
};

ShaderManager.prototype.setLightActive = function(gl, index, value)
{
    this.currentShader.setLightActive(gl, index, value);
};

ShaderManager.prototype.setTexture = function(gl, value)
{
    this.currentShader.setTexture(gl, value);
};
function TexturePool(maxTextureCount)
{
    this.textures = new GenericPool(Texture.prototype, maxTextureCount);
}

TexturePool.prototype.addTexture = function(gl, image, format, width, height)
{
    var texture = this.textures.activateObject();

    // Initialize texture:
    texture.initialize(gl, image, format, width, height);

    return texture.index;
};
TexturePool.prototype.addTextureWithFile = function(gl, image, format)
{
    var texture = this.textures.activateObject();

    // Initialize texture:
    texture.initializeWithFile(gl, image, format);

    return texture.index;
};
TexturePool.prototype.removeTexture = function(gl, index)
{
    var texture = this.textures.deactivateObject(index);

    // Shutdown texture:
    texture.shutdown(gl);
};
TexturePool.prototype.bindTexture = function(gl, index, textureShaderLocation, textureShaderIndex)
{
    // Bind texture with the given index:
    this.textures.getObject(index).bind(gl, textureShaderLocation, textureShaderIndex);
};
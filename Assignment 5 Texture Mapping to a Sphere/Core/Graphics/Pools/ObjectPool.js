function ObjectPool(maxLightCount, maxModelCount)
{
    this.modelPool = new ModelPool(10);
    this.texturePool = new TexturePool(10);
    this.transformsPool = new TransformPool(10);

    // Initialize lights:
    this.lightInstancesPool = new GenericPool(LightInstance.prototype, maxLightCount);
    this.lightPool = new LightsPool(maxLightCount);
    this.maxLightCount = maxLightCount;

    // Initialize texture model instances:
    this.textureModelInstancePool = new GenericPool(TextureModelInstance.prototype, 10);

    // Initialize double texture model instances:
    this.doubleTextureModelInstancePool = new GenericPool(DoubleTextureModelInstance.prototype, 10);
}

ObjectPool.prototype.addLight = function(gl, shaderManager, position, diffuseColor, specularColor, ambientColor)
{
    var lightId = this.lightPool.addLight(gl, shaderManager, position, diffuseColor, specularColor, ambientColor);
    var transformId = this.transformsPool.addTransform(PointLight.createTransform(position));

    var instance = this.lightInstancesPool.activateObject();
    instance.initialize(lightId, 0, transformId); // TODO add modelId

    return instance.index;
};
ObjectPool.prototype.removeLight = function(gl, shaderManager, index)
{
    var instance = this.lightInstancesPool.deactivateObject(index);

    this.transformsPool.removeTransform(instance.transformId);
    this.lightPool.removeLight(gl, shaderManager, instance.lightId);
};
ObjectPool.prototype.renderLights = function(gl, shaderManager)
{
    for(var i = 0; i < this.maxLightCount; i++)
    {
        var instance = this.lightInstancesPool;
        var light = this.lightPool.getLight(instance.lightId);

        if(!light.isActive())
            continue;

        this.lightPool.renderLight(gl, shaderManager, instance.lightId);
        this.transformsPool.renderTransform(gl, shaderManager, instance.transformId);
        // TODO render model
    }
};
ObjectPool.prototype.setLightPosition = function(index, value)
{
    var instance = this.lightInstancesPool.getObject(index);

    this.lightPool.setLightPosition(instance.lightId, value);
    this.transformsPool.setTransform(
        instance.transformId,
        PointLight.createTransform(value)
    );
};
ObjectPool.prototype.setLightDiffuseColor = function(index, value)
{
    var instance = this.lightInstancesPool.getObject(index);
    this.lightPool.setLightDiffuseColor(instance.lightId, value);
};
ObjectPool.prototype.setLightSpecularColor = function(index, value)
{
    var instance = this.lightInstancesPool.getObject(index);
    this.lightPool.setLightSpecularColor(instance.lightId, value);
};
ObjectPool.prototype.setLightAmbientColor = function(index, value)
{
    var instance = this.lightInstancesPool.getObject(index);
    this.lightPool.setLightAmbientColor(instance.lightId, value);
};
ObjectPool.prototype.getLight = function(index)
{
    var instance = this.lightInstancesPool.getObject(index);
    return this.lightPool.getLight(instance.lightId);
};
ObjectPool.prototype.setLightActive = function(gl, shaderManager, index, value)
{
    var instance = this.lightInstancesPool.getObject(index);
    this.lightPool.setLightActive(gl, shaderManager, instance.lightId, value);
};

ObjectPool.prototype.addTextureModel = function(gl, textureShader, vertices, normals, textureCoordinates, indices)
{
    return this.modelPool.addTextureModel(gl, textureShader, vertices, normals, textureCoordinates, indices);
};

ObjectPool.prototype.addTransform = function(value)
{
    return this.transformsPool.addTransform(value);
};
ObjectPool.prototype.setTransform = function(index, value)
{
    this.transformsPool.setTransform(index, value);
};

ObjectPool.prototype.addTexture = function(gl, image, format, width, height)
{
    return this.texturePool.addTexture(gl, image, format, width, height);
};
ObjectPool.prototype.addTextureWithFile = function(gl, image, format)
{
    return this.texturePool.addTextureWithFile(gl, image, format);
};

ObjectPool.prototype.addTextureModelInstance = function(modelId, transformId, textureId)
{
    var textureModelInstance = this.textureModelInstancePool.activateObject();

    textureModelInstance.initialize(modelId, transformId, textureId);

    return textureModelInstance.index;
};

ObjectPool.prototype.renderTextureModelInstance = function(gl, shaderManager, index,  shaderLocation, shaderIndex)
{
    var instance = this.textureModelInstancePool.getObject(index);

    this.texturePool.bindTexture(gl, instance.textureId, shaderLocation, shaderIndex);
    this.transformsPool.renderTransform(gl, shaderManager, instance.transformId);
    this.modelPool.renderTextureModel(gl, instance.modelId);
};

ObjectPool.prototype.addDoubleTextureModelInstance = function(modelId, transformId, texture0Id, texture1Id)
{
    var doubleTextureModelInstance = this.doubleTextureModelInstancePool.activateObject();

    doubleTextureModelInstance.initialize(modelId, transformId, texture0Id, texture1Id);

    return doubleTextureModelInstance.index;
};

ObjectPool.prototype.renderDoubleTextureModelInstance = function(gl, shaderManager, index, texture0ShaderLocation, texture0ShaderIndex, texture1ShaderLocation, texture1ShaderIndex)
{
    var instance = this.doubleTextureModelInstancePool.getObject(index);

    this.texturePool.bindTexture(gl, instance.texture0Id, texture0ShaderLocation, texture0ShaderIndex);
    this.texturePool.bindTexture(gl, instance.texture1Id, texture1ShaderLocation, texture1ShaderIndex);
    this.transformsPool.renderTransform(gl, shaderManager, instance.transformId);
    this.modelPool.renderTextureModel(gl, instance.modelId);
};
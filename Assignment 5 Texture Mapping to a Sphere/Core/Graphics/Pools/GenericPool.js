function GenericPool(prototype, maxSize)
{
    this.objects = [];
    this.objects.length = maxSize;

    for(var i = 0; i < maxSize; i++)
    {
        this.objects[i] = Object.create(prototype);
        this.objects[i].index = i;
        this.objects[i].nextAvailableIndex = i + 1;
    }

    this.objects[maxSize - 1].nextAvailableIndex = null;

    this.firstAvailableIndex = 0;
}

GenericPool.prototype.activateObject = function()
{
    if(this.firstAvailableIndex === null)
        throw new Error("The pool is full!");

    // Get first active index:
    var index = this.firstAvailableIndex;

    // Get first object available:
    var object = this.objects[index];

    // Update first available index and set next available index to null:
    this.firstAvailableIndex = object.nextAvailableIndex;
    object.nextAvailableIndex = null;

    return object;
};
GenericPool.prototype.deactivateObject = function(index)
{
    // Get object which will be deactivated:
    var object = this.objects[index];

    // Update next available index and first available index:
    object.nextAvailableIndex = this.firstAvailableIndex;
    this.firstAvailableIndex = index;

    return object;
};

GenericPool.prototype.getObject = function(index)
{
    return this.objects[index];
};
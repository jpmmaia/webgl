function TransformPool(maxSize)
{
    this.transforms = new GenericPool(TransformComponent.prototype, maxSize);
}

TransformPool.prototype.addTransform = function(value)
{
    var transform = this.transforms.activateObject();

    // Initialize transform:
    transform.initialize(value);

    return transform.index;
};
TransformPool.prototype.removeTransform = function(index)
{
    var transform = this.transforms.deactivateObject(index);

    // Shutdown transform:
    transform.shutdown();
};
TransformPool.prototype.setTransform = function(index, value)
{
    // Update transform value:
    this.transforms.getObject(index).setTransform(value);
};

TransformPool.prototype.renderTransform = function(gl, shaderManager, index)
{
    // Render transform:
    var transform = this.transforms.getObject(index).render(gl, shaderManager);
};
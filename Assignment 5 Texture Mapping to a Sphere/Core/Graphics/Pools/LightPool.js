function LightsPool(maxSize)
{
    this.lights = new GenericPool(PointLight.prototype, maxSize);
}

LightsPool.prototype.addLight = function(gl, shaderManager, position, diffuseColor, specularColor, ambientColor)
{
    var light = this.lights.activateObject();

    // Initialize light:
    light.initialize(gl, shaderManager, index, position, diffuseColor, specularColor, ambientColor);

    return light.index;
};
LightsPool.prototype.removeLight = function(gl, shaderManager, index)
{
    var light = this.lights.deactivateObject(index);

    // Shutdown light:
    light.shutdown(gl, shaderManager);
};

LightsPool.prototype.renderLight = function(gl, shaderManager, index)
{
    // Render light:
    this.lights.getObject(index).render(gl, shaderManager);
};

LightsPool.prototype.getLight = function(index)
{
    return this.lights[index];
};

LightsPool.prototype.setLight = function(index, position, diffuseColor, specularColor, ambientColor)
{
    // Get light which will be set:
    var light = this.lights.getObject(index);

    // Update light parameters:
    light.setParameters(position, diffuseColor, specularColor, ambientColor);
};

LightsPool.prototype.setLightPosition = function(index, value)
{
    // Get light which will be set:
    var light = this.lights.getObject(index);

    // Update light position:
    light.setPosition(value);
};
LightsPool.prototype.setLightDiffuseColor = function(index, value)
{
    // Get light which will be set:
    var light = this.lights.getObject(index);

    // Update light diffuse color:
    light.setDiffuseColor(value);
};
LightsPool.prototype.setLightSpecularColor = function(index, value)
{
    // Get light which will be set:
    var light = this.lights.getObject(index);

    // Update light specular color:
    light.setSpecularColor(value);
};
LightsPool.prototype.setLightAmbientColor = function(index, value)
{
    // Get light which will be set:
    var light = this.lights.getObject(index);

    // Update light ambient color:
    light.setAmbientColor(value);
};
LightsPool.prototype.setLightActive = function(gl, shaderManager, index, value)
{
    // Get light which will be set:
    var light = this.lights.getObject(index);

    // Update light active variable:
    light.setActive(gl, shaderManager, value);
};
function ModelPool(maxTextureModelsCount)
{
    this.textureModels = new GenericPool(TextureModel.prototype, maxTextureModelsCount);
}

ModelPool.prototype.addTextureModel = function(gl, textureShader, vertices, normals, textureCoordinates, indices)
{
    var textureModel = this.textureModels.activateObject();

    // Initialize model:
    textureModel.initialize(gl, textureShader, vertices, normals, textureCoordinates, indices);

    return textureModel.index;
};
ModelPool.prototype.removeTextureModel = function(gl, index)
{
    var textureModel = this.textureModels.deactivateObject(index);

    // Shutdown model:
    textureModel.shutdown(gl);
};
ModelPool.prototype.renderTextureModel = function(gl, index)
{
    // Render model with the given index:
    this.textureModels.getObject(index).render(gl);
};
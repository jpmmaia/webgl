function DoubleTextureModelInstance()
{
    this.modelId = 0;
    this.transformId = 0;
    this.texture0Id = 0;
    this.texture1Id = 0;
}

DoubleTextureModelInstance.prototype.initialize = function(modelId, transformId, texture0Id, texture1Id)
{
    this.modelId = modelId;
    this.transformId = transformId;
    this.texture0Id = texture0Id;
    this.texture1Id = texture1Id;
};
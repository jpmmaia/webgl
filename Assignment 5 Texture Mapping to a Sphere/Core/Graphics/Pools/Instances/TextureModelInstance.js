function TextureModelInstance()
{
    this.modelId = 0;
    this.transformId = 0;
    this.textureId = 0;
}

TextureModelInstance.prototype.initialize = function(modelId, transformId, textureId)
{
    this.modelId = modelId;
    this.transformId = transformId;
    this.textureId = textureId;
};
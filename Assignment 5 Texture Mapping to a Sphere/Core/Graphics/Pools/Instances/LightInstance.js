function LightInstance()
{
    this.shutdown();
}

LightInstance.prototype.initialize = function(lightId, modelId, transformId)
{
    this.lightId = 0;
    this.modelId = modelId;
    this.transformId = transformId;
};
LightInstance.prototype.shutdown = function()
{
    this.lightId = 0;
    this.modelId = 0;
    this.transformId = 0;
};
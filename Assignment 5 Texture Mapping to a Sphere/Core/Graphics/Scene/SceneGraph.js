function SceneGraph()
{

}

SceneGraph.prototype.initialize = function()
{
    this.identityMatrix = mat4(1);

    // Create an array to hold all nodes:
    this.nodes = [];

    // Add a root node:
    this.rootNode = new Node();
    this.rootNode.initialize();
    this.nodes["Root"] = this.rootNode;
};

SceneGraph.prototype.update = function()
{
    this.rootNode.update(this.identityMatrix, false);
};

SceneGraph.prototype.addNode = function(transformComponent, parentId)
{
    // Create a new node:
    var node = new Node();
    node.initialize(transformComponent, parentId);

    // Add node to parent:
    this.nodes[parentId].addChild(node);

    this.nodes[transformComponent.getId()] = node;
};

SceneGraph.prototype.removeNode = function(id)
{
    var node = this.nodes[id];

    var parentNode = this.nodes[node.parentId];
    parentNode.removeChild(id);
};

SceneGraph.prototype.getNode = function(id)
{
    return this.nodes[id];
};
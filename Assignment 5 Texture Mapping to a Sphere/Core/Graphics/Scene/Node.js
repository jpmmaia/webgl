function Node()
{
}

Node.prototype.initialize = function(transformComponent, parentId)
{
    this.dirty = true;
    this.localTransform = mat4(1);
    this.globalTransform = mat4(1);
    this.childNodes = [];
    this.transformComponent = transformComponent;
    this.parentId = parentId;
};

Node.prototype.update = function(fatherTransform, dirty)
{
    dirty |= this.dirty;
    if(dirty)
    {
        this.dirty = false;

        // Update global transform:
        this.globalTransform = mult(fatherTransform, this.localTransform);

        // Set value of transform component:
        this.transformComponent.setTransform(this.globalTransform);
    }

    // Update all child nodes:
    for(var i = 0; i < this.childNodes.length; i++)
        this.childNodes[i].update(this.globalTransform, dirty);
};

Node.prototype.setLocalTransform = function(transform)
{
    this.localTransform = transform;

    this.dirty = true;
};

Node.prototype.setTransformComponent = function(value)
{
    this.transformComponent = value;
};

Node.prototype.addChild = function(node)
{
    // Add child node:
    this.childNodes.push(node);

    // Update node with new global transform:
    node.update(this.globalTransform, true);
};

Node.prototype.removeChild = function(id)
{
    for(var i = 0; i < this.childNodes.length; i++)
    {
        var node = this.childNodes[i];

        if(node.transformComponent.getId() === id)
        {
            this.childNodes.splice(i, 1);
        }
    }
};
function Scene()
{
    this.maxLightCount = 4;
    this.maxSceneObjectCount = 50;
    this.sceneGraph = new SceneGraph();
    this.objectPool = new ObjectPool(this.maxLightCount, this.maxSceneObjectCount);

    this.deltaAngle = 0.1;
}

Scene.prototype.initialize = function(gl, shaderManager)
{
    // Set clear color:
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // Initialize scene graph:
    this.sceneGraph.initialize();

    this.objectCount = 0;

    // Create cameras array:
    this.cameras = [];

    this.initializeObjects(gl, shaderManager);
};

Scene.prototype.shutdown = function(gl)
{
};

Scene.prototype.update = function(elapsedTime, deltaTime)
{
};

Scene.prototype.render = function(gl, shaderManager)
{
    // Set shader:
    //shaderManager.setTextureShader(gl);

    // Set shader parameters:
    //shaderManager.setCameraPosition(gl, flatten(this.currentCamera.getPosition()));

    // Render all lights:
    //this.objectPool.renderLights(gl, shaderManager);

    // Render all scene objects:
    //this.texture.bind(gl);
    //this.objectPool.renderSceneObjects(gl, shaderManager);
    //this.objectPool.renderTextureModelInstance(gl, shaderManager, 0);

    if(this.currentObject !== null)
    {
        if(this.currentObject === "FireDoubleTextureModel" || this.currentObject == "GlobeDoubleTextureModel")
        {
            shaderManager.setDoubleTextureShader(gl);

            this.beginRender(gl, shaderManager);

            var doubleTextureShader = shaderManager.doubleTextureShader;
            this.objectPool.renderDoubleTextureModelInstance(gl, shaderManager, this.objects[this.currentObject], doubleTextureShader.texture0, 0, doubleTextureShader.texture1, 1);
        }
        else
        {
            shaderManager.setTextureShader(gl);

            this.beginRender(gl, shaderManager);

            this.objectPool.renderTextureModelInstance(gl, shaderManager, this.objects[this.currentObject], shaderManager.textureShader.texture, 0);
        }
    }
};
Scene.prototype.beginRender = function(gl, shaderManager)
{
    shaderManager.setProjectionMatrix(gl, flatten(this.currentCamera.getProjectionMatrix()));
    this.dirtyProjectionMatrix = false;

    // Set view matrix:
    shaderManager.setViewMatrix(gl, flatten(this.currentCamera.getViewMatrix()));
};

Scene.prototype.addCamera = function(position)
{
    // Create an ID:
    var id = this.objectCount++;

    // Create a camera component:
    var cameraComponent = new CameraComponent();
    cameraComponent.initialize(id);

    // Set position:
    cameraComponent.translate(position[0], position[1], position[2]);

    // Add camera to hash table:
    this.cameras[id] = cameraComponent;

    return id;
};

Scene.prototype.addSphere = function()
{
    return this.objectPool.addSphere(mat4(1));
};
Scene.prototype.addCylinder = function()
{
    return this.objectPool.addCylinder(mat4(1));
};
Scene.prototype.addCone = function()
{
    return this.objectPool.addCone(mat4(1));
};
Scene.prototype.removeSceneObject = function(index)
{
    this.objectPool.removeSceneObject(index);
};

Scene.prototype.addLight = function(gl, shaderManager, position, diffuseColor, specularColor, ambientColor)
{
    return this.objectPool.addLight(gl, shaderManager, position, diffuseColor, specularColor, ambientColor);
};
Scene.prototype.removeLight = function(gl, shaderManager, index)
{
    this.objectPool.removeLight(gl, shaderManager, index);
};

Scene.prototype.setActiveCamera = function(id)
{
    this.currentCamera = this.cameras[id];
};
Scene.prototype.setOrthogonalMode = function(aspectRatio)
{
    this.currentCamera.setOrthogonal(-3.0 * aspectRatio, 3.0 * aspectRatio, -3.0, 3.0, -20.0, 20.0);
};
Scene.prototype.setPerspectiveMode = function(fieldOfViewY, aspectRatio)
{
    this.currentCamera.setPerspective(fieldOfViewY, aspectRatio, 0.1, 20.0);
};

Scene.prototype.getLight = function(id)
{
    return this.objectPool.getLight(id);
};

Scene.prototype.setLightPosition = function(id, value)
{
    this.objectPool.setLightPosition(id, value);
};
Scene.prototype.setLightDiffuseColor = function(id, value)
{
    this.objectPool.setLightDiffuseColor(id, value);
};
Scene.prototype.setLightSpecularColor = function(id, value)
{
    this.objectPool.setLightSpecularColor(id, value);
};
Scene.prototype.setLightAmbientColor = function(id, value)
{
    this.objectPool.setLightAmbientColor(id, value);
};
Scene.prototype.setLightActive = function(gl, shaderManager, index, value)
{
    this.objectPool.setLightActive(gl, shaderManager, index, value);
};

Scene.prototype.setSceneObjectTransform = function(value)
{
    this.objectPool.setTransform(this.objects["MainTransform"], value);
};

Scene.prototype.setGlobalAmbientColor = function(gl, shaderManager, value)
{
    shaderManager.setGlobalAmbientColor(gl, value);
};

Scene.prototype.setLightLinearAttenuation = function(gl, shaderManager, value)
{
    shaderManager.setLinearAttenuation(gl, value);
};

Scene.prototype.setLightSquareAttenuation = function(gl, shaderManager, value)
{
    shaderManager.setSquareAttenuation(gl, value);
};

Scene.prototype.initializeObjects = function(gl, shaderManager)
{
    this.objects = {};
    this.currentObject = null;

    var options = {};


    var sphere = new Sphere();

    // Create texture model with a sphere with planar texture mapping:
    options[sphere.TEXTURE_MAPPING_MODE] = sphere.TEXTURE_MAPPING_MODE_PLANAR;
    sphere.initialize(32, 16, options);
    this.objects["PlanarMappingTextureModel"] = this.objectPool.addTextureModel(gl, shaderManager.textureShader, sphere.vertices, sphere.normals, sphere.textureCoordinates, sphere.indices);

    // Create texture model with a sphere with spherical texture mapping:
    options[sphere.TEXTURE_MAPPING_MODE] = sphere.TEXTURE_MAPPING_MODE_SPHERICAL;
    sphere.initialize(32, 16, options);
    this.objects["SphericalMappingTextureModel"] = this.objectPool.addTextureModel(gl, shaderManager.textureShader, sphere.vertices, sphere.normals, sphere.textureCoordinates, sphere.indices);

    var square = new Square();
    square.initialize();
    this.objects["SquareTextureModel"] = this.objectPool.addTextureModel(gl, shaderManager.textureShader, square.vertices, square.normals, square.textureCoordinates, square.indices);

    // Create main transform:
    this.objects["MainTransform"] = this.objectPool.addTransform(mat4(1));

    // Create checkerboard texture:
    var checkerboardImage = Texture.generateCheckerboardImage(gl, 256);
    this.objects["CheckerboardTexture"] = this.objectPool.addTexture(gl, checkerboardImage, gl.RGBA, 256, 256);

    // Create globe texture:
    this.addFileTexture("GlobeTexture", gl, shaderManager, "../Resources/globe_image.jpg", gl.RGBA);

    // Create fire and ice texture:
    this.addFileTexture("FireAndIceTexture", gl, shaderManager, "../Resources/fire_and_ice_image.jpg", gl.RGBA);

    // Create perlin noise texture:
    var noiseImage = PerlinNoise.generateImage(133522, 0.7, 512, 6);
    this.objects["PerlinNoiseTexture"] = this.objectPool.addTexture(gl, noiseImage, gl.RGBA, 512, 512);

    this.addTextureModel("PlanarCheckerboardSphere", "PlanarMappingTextureModel", "MainTransform", "CheckerboardTexture");
    this.addTextureModel("PlanarGlobeSphere", "PlanarMappingTextureModel", "MainTransform", "GlobeTexture");

    this.addTextureModel("SphericalCheckerboardSphere", "SphericalMappingTextureModel", "MainTransform", "CheckerboardTexture");
    this.addTextureModel("SphericalGlobeSphere", "SphericalMappingTextureModel", "MainTransform", "GlobeTexture");
    this.addDoubleTextureModel("GlobeDoubleTextureModel", "SphericalMappingTextureModel", "MainTransform", "GlobeTexture", "CheckerboardTexture");

    this.addTextureModel("SphericalNoiseGlobeSphere", "SphericalMappingTextureModel", "MainTransform", "PerlinNoiseTexture");
    this.addTextureModel("SphericalFireAndIceSphere", "SphericalMappingTextureModel", "MainTransform", "FireAndIceTexture");
    this.addDoubleTextureModel("FireDoubleTextureModel", "SphericalMappingTextureModel", "MainTransform", "FireAndIceTexture", "PerlinNoiseTexture");

    //this.addTextureModel("Square", "SquareTextureModel", "MainTransform", "PerlinNoiseTexture");

    this.currentObject = "SphericalFireAndIceSphere";
};

Scene.prototype.addFileTexture = function(textureId, gl, shaderManager, source, format)
{
    var image = new Image();
    image.src = source;
    image.onload = this.addTextureWithFile(textureId, gl, shaderManager, image, format);
};

Scene.prototype.addTextureWithFile = function(textureId, gl, shaderManager, image, format)
{
    this.objects[textureId] = this.objectPool.addTextureWithFile(gl, image, format);
};

Scene.prototype.addTextureModel = function(objectId, modelId, transformId, textureId)
{
    this.objects[objectId] = this.objectPool.addTextureModelInstance(this.objects[modelId], this.objects[transformId], this.objects[textureId]);
};

Scene.prototype.addDoubleTextureModel = function(objectId, modelId, transformId, texture0Id, texture1Id)
{
    this.objects[objectId] = this.objectPool.addDoubleTextureModelInstance(this.objects[modelId], this.objects[transformId], this.objects[texture0Id], this.objects[texture1Id]);
};

Scene.prototype.setCurrentObject = function(id)
{
    this.currentObject = id;
};
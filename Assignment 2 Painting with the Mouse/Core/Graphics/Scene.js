function Scene()
{
}

Scene.prototype.initialize = function(gl, shaderManager)
{
    // Set clear color:
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // Create a line model array:
    this.lineModels = [];

    this.maxVertexCountPerLineModel = 2000;
    this.antiAliasedLines = false;
};

Scene.prototype.shutdown = function(gl)
{
    for(var i = 0; i < this.lineModels.length; i++)
        this.lineModels[i].shutdown(gl);

    // Clear list of line models:
    this.lineModels.length = 0;
};

Scene.prototype.render = function(gl, shaderManager)
{
    if(this.antiAliasedLines)
        shaderManager.setAntiAliasedLineShader(gl);
    else
        shaderManager.setLineShader(gl);

    for(var i = 0; i < this.lineModels.length; i++)
        this.lineModels[i].render(gl);
};

Scene.prototype.startNewLine = function(gl, shaderManager)
{
    var lineModel = new LineModel();
    lineModel.initialize(gl, shaderManager.lineShader, this.maxVertexCountPerLineModel);

    this.lineModels.push(lineModel);
};

Scene.prototype.addPoint = function(gl, shaderManager, position, color, width)
{
    var lineModel = this.lineModels[this.lineModels.length - 1];

    // If line model has reached its maximum vertex count capacity:
    if(lineModel.vertexCount == lineModel.maxVertexCount)
    {
        // Create a new line model:
        lineModel = new LineModel();
        lineModel.initialize(gl, shaderManager.lineShader, this.maxVertexCountPerLineModel);

        // Add it to the list:
        this.lineModels.push(lineModel);
    }

    // Add a point to the line:
    lineModel.addPoint(gl, position, color, width);
};

Scene.prototype.enableAntiAliasedLines = function(value)
{
    this.antiAliasedLines = value;
};
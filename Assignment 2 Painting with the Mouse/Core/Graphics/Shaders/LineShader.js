function LineShader()
{
    this.super = new AbstractShader();
}

LineShader.prototype.initialize = function(gl)
{
    this.super.initialize(gl, "LineVertexShader", "LineFragmentShader");

    // Get attributes location:
    this.position = gl.getAttribLocation(this.super.program, "vPosition");
    this.color = gl.getAttribLocation(this.super.program, "vColor");
    this.offsetVector = gl.getAttribLocation(this.super.program, "vOffsetVector");
    this.width = gl.getAttribLocation(this.super.program, "vWidth");
};

LineShader.prototype.setShader = function(gl)
{
    this.super.setShader(gl);
};
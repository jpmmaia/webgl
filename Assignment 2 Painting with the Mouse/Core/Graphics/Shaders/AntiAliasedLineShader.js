function AntiAliasedLineShader()
{
    this.super = new AbstractShader();
}

AntiAliasedLineShader.prototype.initialize = function(gl, feather)
{
    this.super.initialize(gl, "AntiAliasedLineVertexShader", "AntiAliasedLineFragmentShader");

    // Get attributes location:
    this.position = gl.getAttribLocation(this.super.program, "vPosition");
    this.color = gl.getAttribLocation(this.super.program, "vColor");
    this.offsetVector = gl.getAttribLocation(this.super.program, "vOffsetVector");
    this.width = gl.getAttribLocation(this.super.program, "vWidth");
    this.feather = gl.getUniformLocation(this.super.program, "uFeather");

    this.setFeather(gl, feather);
};

AntiAliasedLineShader.prototype.setShader = function(gl)
{
    this.super.setShader(gl);
};

AntiAliasedLineShader.prototype.setFeather = function(gl, value)
{
    this.super.setShader(gl);
    gl.uniform1f(this.feather, value);
};
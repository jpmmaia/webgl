function ShaderManager()
{
}

ShaderManager.prototype.initialize = function(gl)
{
    // Initialize line shader:
    this.lineShader = new LineShader();
    this.lineShader.initialize(gl);

    // Initialize anti-aliased line shader:
    this.antiAliasedLineShader = new AntiAliasedLineShader();
    this.antiAliasedLineShader.initialize(gl, 0.5);
};

ShaderManager.prototype.setLineShader = function(gl)
{
    this.lineShader.setShader(gl);
};

ShaderManager.prototype.setAntiAliasedLineShader = function(gl)
{
    this.antiAliasedLineShader.setShader(gl);
};
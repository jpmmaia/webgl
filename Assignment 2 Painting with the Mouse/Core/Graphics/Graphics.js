function Graphics()
{
}

Graphics.prototype.initialize = function(canvas)
{
    // Get canvas element:
    this.canvas = canvas;

    // Initialize WebGL:
    this.gl = WebGLUtils.setupWebGL(canvas);
    if(!this.gl)
        alert("WebGL isn't available");

    // Initialize shader manager:
    this.shaderManager = new ShaderManager();
    this.shaderManager.initialize(this.gl);

    // Setup blending:
    this.gl.enable(this.gl.BLEND);
    this.gl.blendFunc (this.gl.SRC_ALPHA, this.gl.ONE_MINUS_SRC_ALPHA);
};

Graphics.prototype.beginScene = function()
{
    // Clear screen:
    this.gl.clear(this.gl.COLOR_BUFFER_BIT);
};
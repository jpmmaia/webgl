function LineModel()
{
}

LineModel.prototype.initialize = function(gl, lineShader, maxVertexCount)
{
    this.vertexCount = 0;
    this.maxVertexCount = maxVertexCount;
    this.points = [];

    // Create a vertex buffer:
    this.vertexBuffer = new GLBuffer();
    this.vertexBuffer.initialize(gl, gl.ARRAY_BUFFER, maxVertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 3, lineShader.position);

    // Create a color buffer:
    this.colorBuffer = new GLBuffer();
    this.colorBuffer.initialize(gl, gl.ARRAY_BUFFER, maxVertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 4, lineShader.color);

    // Create a offset vector buffer:
    this.offsetVectorBuffer = new GLBuffer();
    this.offsetVectorBuffer.initialize(gl, gl.ARRAY_BUFFER, maxVertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 3, lineShader.offsetVector);

    // Create a width buffer:
    this.widthBuffer = new GLBuffer();
    this.widthBuffer.initialize(gl, gl.ARRAY_BUFFER, maxVertexCount, gl.STATIC_DRAW, gl.FLOAT, 4, 1, lineShader.width);
};

LineModel.prototype.shutdown = function(gl)
{
    this.widthBuffer.shutdown(gl);
    this.offsetVectorBuffer.shutdown(gl);
    this.colorBuffer.shutdown(gl);
    this.vertexBuffer.shutdown(gl);
};

LineModel.prototype.render = function(gl)
{
    // Bing buffers so that they are used in the shader:
    this.vertexBuffer.bind(gl);
    this.colorBuffer.bind(gl);
    this.offsetVectorBuffer.bind(gl);
    this.widthBuffer.bind(gl);

    // Draw line:
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, this.vertexCount);
};

LineModel.prototype.addPoint = function(gl, position, color, width)
{
    // The offset vector can only be calculated having at least 2 points:
    if(this.points.length == 0)
    {
        if(!this.addVertex(gl, position, color, [0, 0, 0], width))
            return false;

        if(!this.addVertex(gl, position, color, [0, 0, 0], width))
            return false;

        this.points.push(position);
        return true;
    }

    // Calculate the perpendicular to the direction of the line segment:
    var v1 = this.points[this.points.length - 1];
    var direction1 = VectorUtils.normalize(VectorUtils.subtract(position, v1));
    var perpendicular1 = VectorUtils.perpendicular(direction1);

    // Add offset vector corresponding to the first point of the line:
    if(this.points.length == 1)
    {
        this.updateOffsetVector(gl, perpendicular1, 0);
        this.updateOffsetVector(gl, VectorUtils.subtract([0, 0, 0], perpendicular1), 1);
    }

    // Update previous offset vector:
    else
    {
        var v0 = this.points[this.points.length - 2];
        var direction0 = VectorUtils.normalize(VectorUtils.subtract(v1, v0));
        var newDirection = VectorUtils.normalize(VectorUtils.add(direction0,  direction1));
        var newPerpendicular = VectorUtils.perpendicular(newDirection);

        this.updateOffsetVector(gl, newPerpendicular, this.vertexCount - 2);
        this.updateOffsetVector(gl, VectorUtils.subtract([0, 0, 0], newPerpendicular), this.vertexCount - 1);

        var newWidth = width / VectorUtils.dot(perpendicular1, newPerpendicular);
        this.updateWidth(gl, width, this.vertexCount - 2);
        this.updateWidth(gl, width, this.vertexCount - 1);
    }

    if(!this.addVertex(gl, position, color, perpendicular1, width))
        return false;

    if(!this.addVertex(gl, position, color, VectorUtils.subtract([0, 0, 0], perpendicular1), width))
        return false;

    this.points.push(position);

    return true;
};

LineModel.prototype.addVertex = function(gl, position, color, offsetVector, width)
{
    if(
        !this.vertexBuffer.add(gl, flatten(position)) ||
        !this.colorBuffer.add(gl, flatten(color)) ||
        !this.offsetVectorBuffer.add(gl, flatten(offsetVector)) ||
        !this.widthBuffer.add(gl, new Float32Array([width]))
    )
        return false;

    this.vertexCount++;

    return true;
};

LineModel.prototype.updateOffsetVector = function(gl, offsetVector, index)
{
    this.offsetVectorBuffer.set(gl, flatten(offsetVector), index);
};

LineModel.prototype.updateWidth = function(gl, width, index)
{
    this.widthBuffer.set(gl, new Float32Array([width]), index);
};
function PolygonModel()
{
    this.super = new AbstractModel();
}

PolygonModel.prototype.initialize = function(gl, positionAttribLocation, numberOfSides)
{
    var radius = 0.5;
    var initialAngle = Math.PI / 2.0;
    var deltaAngle = 2.0 * Math.PI / numberOfSides;

    var vertices = [];
    for(var i = 0; i < numberOfSides; i++)
    {
        var angle = initialAngle + i * deltaAngle;

        var x = radius * Math.cos(angle);
        var y = radius * Math.sin(angle);
        var z = 0.0;

        vertices.push(x, y, z);
    }

    this.vertices = [];
    for(i = 0; i < numberOfSides; i++)
    {
        var index1 = i * 3;
        var index2 = ((i + 1) * 3) % vertices.length;

        this.vertices.push(0.0, 0.0, 0.0);
        this.vertices.push(vertices[index2], vertices[index2 + 1], vertices[index2 + 2]);
        this.vertices.push(vertices[index1], vertices[index1 + 1], vertices[index1 + 2]);
    }

    this.vertexCount = this.vertices.length / 3;

    this.super.initialize(gl, positionAttribLocation, this.vertices);
};

PolygonModel.prototype.render = function(gl)
{
    this.super.render(gl);

    gl.drawArrays(gl.TRIANGLES, 0, this.vertexCount);
};
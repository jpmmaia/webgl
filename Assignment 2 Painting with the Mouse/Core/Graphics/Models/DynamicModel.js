function DynamicModel()
{
    this.super = new AbstractModel();
}

DynamicModel.prototype.initialize = function(gl, positionAttribLocation, colorAttribLocation, maxVertexCount)
{
    this.maxVertexCount = maxVertexCount;
    this.vertexCount = 0;

    this.super.initialize(gl, positionAttribLocation, colorAttribLocation, maxVertexCount * 12, maxVertexCount * 16);
};

DynamicModel.prototype.shutdown = function(gl)
{
    this.super.shutdown(gl);
};

DynamicModel.prototype.render = function(gl)
{
    this.super.render(gl);

    // Draw triangle:
    gl.drawArrays(gl.LINE_STRIP, 0, this.vertexCount);
};

DynamicModel.prototype.addVertex = function(gl, position, color)
{
    if(this.vertexCount >= this.maxVertexCount)
        return;

    // Add new vertex:
    this.super.setVertex(gl, flatten(position), flatten(color), this.vertexCount);

    // Increment vertex count:
    this.vertexCount++;
};

DynamicModel.prototype.setVertex = function(gl, position, color, index)
{
    if(this.index < 0 || this.index >= this.vertexCount)
        return;

    // Set vertex:
    this.super.setVertex(gl, flatten(position), flatten(color), index);
};
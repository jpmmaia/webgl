var application;

window.onload = function initialize()
{
    // Get canvas element:
    var canvas = document.getElementById("gl-canvas");

    // Initialize application:
    application = new Application();
    application.initialize(canvas);

    // Initialize user interface:
    initializeUserInterface(canvas);
};

function initializeUserInterface(canvas)
{
    canvas.addEventListener("mousedown", onMouseDown);
    canvas.addEventListener("mousemove", onMouseMove);
    canvas.addEventListener("mouseup", onMouseUp);

    var colorInputElement = document.getElementById("colorInput");
    colorInputElement.onchange = onColorChange;

    var clearButtonElement = document.getElementById("clearButton");
    clearButtonElement.onclick = onClearButtonClick;

    var lineWidthInputElement = document.getElementById("lineWidthInput");
    lineWidthInputElement.onchange = onLineWidthChange;

    var antiAliasingInputElement = document.getElementById("antiAliasingInput");
    antiAliasingInputElement.onchange = onAntiAliasingChange;

    var antiAliasingValueInputElement = document.getElementById("antiAliasingValueInput");
    antiAliasingValueInputElement.onchange = onAntiAliasingValueChange;
}

function onMouseDown(event)
{
    application.onMouseDown(event.clientX, event.clientY);
}

function onMouseMove(event)
{
    application.onMouseMove(event.clientX, event.clientY);
}

function onMouseUp(event)
{
    application.onMouseUp(event.clientX, event.clientY);
}

function onColorChange()
{
    var colorInputElement = document.getElementById("colorInput");
    var value = colorInputElement.value;

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(value);
    if(!result)
        return;

    var color =
        [
            parseInt(result[1], 16),
            parseInt(result[2], 16),
            parseInt(result[3], 16)
        ];

    application.setLineColor(color);
}

function onClearButtonClick()
{
    application.clearScene();
}

function onLineWidthChange()
{
    var lineWidthInputElement = document.getElementById("lineWidthInput");
    var value = parseInt(lineWidthInputElement.value);

    application.setLineWidth(value);
}

function onAntiAliasingChange()
{
    var antiAliasingInputElement = document.getElementById("antiAliasingInput");
    var value = antiAliasingInputElement.checked;

    application.enableAntiAliasing(value);
}

function onAntiAliasingValueChange()
{
    var antiAliasingValueInputElement = document.getElementById("antiAliasingValueInput");
    var value = parseFloat(antiAliasingValueInputElement.value);

    application.setAntiAliasingValue(value);
}
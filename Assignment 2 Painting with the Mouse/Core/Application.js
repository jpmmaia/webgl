function Application()
{
}

Application.prototype.initialize = function(canvas)
{
    // Set canvas:
    this.canvas = canvas;

    // Create graphics object:
    this.graphics = new Graphics();
    this.graphics.initialize(canvas);

    // Create a scene:
    this.scene = new Scene();
    this.scene.initialize(this.graphics.gl, this.graphics.shaderManager);

    // Render scene:
    this.render();

    this.currentLineWidth = 0.002;
    this.currentLineColor = [1.0, 0.0, 0.0, 1.0];

    // Set mouse down to false:
    this.mouseDown = false;

    // Set last timestamp to null:
    this.lastTimestamp = null;
};

Application.prototype.render = function()
{
    this.graphics.beginScene();
    this.scene.render(this.graphics.gl, this.graphics.shaderManager);
};

Application.prototype.setLineWidth = function(width)
{
    this.currentLineWidth = width * 0.001;
};

Application.prototype.setLineColor = function(color)
{
    this.currentLineColor = [color[0], color[1], color[2], 1.0];
};

Application.prototype.clearScene = function()
{
    // Shutdown the scene:
    this.scene.shutdown(this.graphics.gl);

    // Apply effects on the screen:
    this.render();
};

Application.prototype.enableAntiAliasing = function(value)
{
    this.scene.enableAntiAliasedLines(value);

    this.render();
};

Application.prototype.setAntiAliasingValue = function(value)
{
    this.graphics.shaderManager.antiAliasedLineShader.setFeather(this.graphics.gl, value);

    this.render();
};

Application.prototype.onMouseDown = function(x, y)
{
    // Start a new line:
    this.scene.startNewLine(this.graphics.gl, this.graphics.shaderManager);

    // Add an initial point:
    this.addPoint(x, y);

    // Set mouse down to true:
    this.mouseDown = true;
};

Application.prototype.onMouseMove = function(x, y)
{
    // If mouse is not down:
    if(!this.mouseDown)
        return;

    // Add a point:
    this.addPoint(x, y);
};

Application.prototype.onMouseUp = function(x, y)
{
    // Add a point:
    this.addPoint(x, y);

    // Set mouse down to false:
    this.mouseDown = false;
};

Application.prototype.windowToClipCoordinates = function(windowCoordinates)
{
    return [
        2.0 * windowCoordinates[0] / this.canvas.width - 1.0,
        2.0 * (this.canvas.height - windowCoordinates[1]) / this.canvas.height - 1.0
    ];
};

Application.prototype.addPoint = function(x, y)
{
    // Calculate the clip coordinates:
    var clipCoordinates = this.windowToClipCoordinates([x, y]);

    // Add point:
    var point = [ clipCoordinates[0], clipCoordinates[1], 0.0 ];
    this.scene.addPoint(this.graphics.gl, this.graphics.shaderManager, point, this.currentLineColor, this.currentLineWidth);

    // Render scene:
    this.render();
};
function Application()
{
}

Application.prototype.initialize = function(canvas, polygonType, tessellationFactor, removeCenter, theta, polygonSides)
{
    this.polygonType = polygonType;
    this.tessellationFactor = tessellationFactor;
    this.removeCenter = removeCenter;
    this.theta = theta;
    this.polygonSides = polygonSides;

    // Create graphics object:
    this.graphics = new Graphics();
    this.graphics.initialize(canvas, polygonType, tessellationFactor, removeCenter, theta, polygonSides);

    // Render scene:
    this.graphics.render();
};

Application.prototype.setPolygonType = function(value)
{
    this.polygonType = value;

    this.updateScreen();
};

Application.prototype.setPolygonSides = function(value)
{
    this.polygonSides = value;

    this.updateScreen();
};

Application.prototype.setTessellationFactor = function(value)
{
    this.tessellationFactor = value;

    this.updateScreen();
};

Application.prototype.setRemoveCenter = function(value)
{
    this.removeCenter = value;

    this.updateScreen();
};

Application.prototype.setTheta = function(value)
{
    this.theta = value;

    this.updateScreen();
};

Application.prototype.updateScreen = function()
{
    this.graphics.setParameters(this.polygonType, this.tessellationFactor, this.removeCenter, this.theta, this.polygonSides);
    this.graphics.render();
};
var application;

window.onload = function initialize()
{
    // Get canvas element:
    var canvas = document.getElementById("gl-canvas");

    // Initialize application:
    application = new Application();
    application.initialize(canvas, "triangle", 1, false, 0, 5);

    // Initialize user interface:
    initializeUserInterface();
};

function initializeUserInterface()
{
    // Add change event listener on polygon type select element:
    var polygonTypeSelectElement = document.getElementById("polygonType");
    polygonTypeSelectElement.onchange = onPolygonTypeChange;

    // Add change event listener on polygon sides input element:
    var polygonSidesInputElement = document.getElementById("polygonSides");
    polygonSidesInputElement.oninput = onPolygonSidesChange;

    // Add change event listener on tessellation factor input element:
    var tessellationFactorInputElement = document.getElementById("tessellationFactor");
    tessellationFactorInputElement.oninput = onTessellationFactorInput;

    // Add change event listener on theta input element:
    var thetaInputElement = document.getElementById("theta");
    thetaInputElement.oninput = onThetaInput;

    // Add change event listener on remove center input element:
    var removeCenterInputElement = document.getElementById("removeCenter");
    removeCenterInputElement.onclick = onRemoveCenterClick;
}

function setPolygonSidesSpanElementVisibility(visible)
{
    var polygonSidesSpanElement = document.getElementById("polygonSidesSpan");

    if(visible)
    {
        // Show elements:
        polygonSidesSpanElement.removeAttribute("hidden");
    }
    else
    {
        // Hide elements:
        polygonSidesSpanElement.setAttribute("hidden", "hidden");
    }
}

function onPolygonTypeChange()
{
    // Get value of polygon type:
    var polygonTypeSelectElement = document.getElementById("polygonType");
    var polygonType = polygonTypeSelectElement.value;

    // Show/hide polygon sides span element:
    setPolygonSidesSpanElementVisibility(polygonType === "generatedPolygon");

    // Set new polygon type:
    application.setPolygonType(polygonType);
}

function onPolygonSidesChange()
{
    // Get value of polygon sides:
    var polygonSidesInputElement = document.getElementById("polygonSides");
    var polygonSides = parseInt(polygonSidesInputElement.value);

    // Update polygon sides value info:
    var polygonSidesValueInfoSpanElement = document.getElementById("polygonSidesValueInfo");
    polygonSidesValueInfoSpanElement.textContent = "(" + polygonSides + ")";

    // Set new polygon sides:
    application.setPolygonSides(polygonSides);
}

function onTessellationFactorInput()
{
    // Get value of tessellation factor:
    var tessellationFactorInputElement = document.getElementById("tessellationFactor");
    var tessellationFactor = parseInt(tessellationFactorInputElement.value);

    // Update tessellation factor value info:
    var tessellationFactorValueInfoSpanElement = document.getElementById("tessellationFactorValueInfo");
    tessellationFactorValueInfoSpanElement.textContent = "(" + tessellationFactor + ")";

    // Set new tessellation factor:
    application.setTessellationFactor(tessellationFactor);
}

function onRemoveCenterClick()
{
    // Get value of remove center:
    var removeCenterInputElement = document.getElementById("removeCenter");
    var removeCenter = removeCenterInputElement.checked;

    // Set new remove center:
    application.setRemoveCenter(removeCenter);
}

function onThetaInput()
{
    // Get value of theta:
    var thetaInputElement = document.getElementById("theta");
    var theta = radians(parseInt(thetaInputElement.value));

    // Set new theta:
    application.setTheta(theta);
}
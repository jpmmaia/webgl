function Graphics()
{
}

Graphics.prototype.initialize = function(canvas, polygonType, tessellationFactor, removeCenter, theta, polygonSides)
{
    // Get canvas element:
    this.canvas = canvas;

    // Initialize WebGL:
    this.gl = WebGLUtils.setupWebGL(canvas);
    if(!this.gl)
        alert("WebGL isn't available");

    // Initialize shader manager:
    this.shaderManager = new ShaderManager();
    this.shaderManager.initialize(this.gl);

    // Initialize scene:
    this.scene = new Scene();
    this.scene.initialize(this.gl, this.shaderManager, polygonType, tessellationFactor, removeCenter, theta, polygonSides);
};

Graphics.prototype.setParameters = function(polygonType, tessellationFactor, removeCenter, theta, polygonSides)
{
    this.scene.setParameters(this.gl, this.shaderManager, polygonType, tessellationFactor, removeCenter, theta, polygonSides);
};

Graphics.prototype.render = function()
{
    // Clear screen:
    this.gl.clear(this.gl.COLOR_BUFFER_BIT);

    // Render scene:
    this.scene.render(this.gl, this.shaderManager);
};
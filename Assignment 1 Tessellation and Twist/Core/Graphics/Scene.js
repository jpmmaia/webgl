function Scene()
{
}

Scene.prototype.initialize = function(gl, shaderManager, polygonType, tessellationFactor, removeCenter, theta, polygonSides)
{
    // Set clear color:
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // Create triangle:
    this.triangle = new Triangle();
    this.initializeTriangle(gl, shaderManager);

    // Create square:
    this.square = new Square();
    this.initializeSquare(gl, shaderManager);

    // Create a generated polygon:
    this.generatedPolygon = new PolygonModel();
    this.initializeGeneratedPolygon(gl, shaderManager, polygonSides);

    // Set parameters:
    this.setParameters(gl, shaderManager, polygonType, tessellationFactor, removeCenter, theta);
};

Scene.prototype.shutdown = function(gl)
{
    this.generatedPolygon.shutdown(gl);
    this.square.shutdown(gl);
    this.triangle.shutdown(gl);
};

Scene.prototype.initializeTriangle = function(gl, shaderManager)
{
    this.triangle.initialize(gl, shaderManager.assignment1Shader.position);
};

Scene.prototype.initializeSquare = function(gl, shaderManager)
{
    this.square.initialize(gl, shaderManager.assignment1Shader.position);
};

Scene.prototype.initializeGeneratedPolygon = function(gl, shaderManager, polygonSides)
{
    this.generatedPolygon.initialize(gl, shaderManager.assignment1Shader.position, polygonSides);
};

Scene.prototype.setParameters = function(gl, shaderManager, polygonType, tessellationFactor, removeCenter, theta, polygonSides)
{
    // If parameters have changed:
    if(this.currentPolygonType !== polygonType ||
        this.currentTessellationFactor !== tessellationFactor ||
        this.currentRemoveCenter !== removeCenter ||
        this.currentPolygonSides !== polygonSides)
    {
        // Initialize a new generated polygon with the given sides:
        if(this.currentPolygonSides !== polygonSides)
            this.initializeGeneratedPolygon(gl, shaderManager, polygonSides);

        // Update current values:
        this.currentPolygonType = polygonType;
        this.currentTessellationFactor = tessellationFactor;
        this.currentRemoveCenter = removeCenter;
        this.currentPolygonSides = polygonSides;

        // Set base polygon:
        var basePolygon;
        switch (polygonType)
        {
            case "triangle":
                basePolygon = this.triangle;
                break;
            case "square":
                basePolygon = this.square;
                break;
            case "generatedPolygon":
                basePolygon = this.generatedPolygon;
                break;
            default:
                return;
        }

        // Create a subdivider to subdivide base polygon:
        var subdivider = new Subdivider();
        subdivider.subdivideModel(basePolygon.vertices, tessellationFactor, removeCenter);

        // Create a model with the generated vertices:
        this.subdividedPolygon = new GenericModel();
        this.subdividedPolygon.initialize(gl, shaderManager.assignment1Shader.positionAttribLocation, subdivider.points);
    }

    // Set theta:
    this.theta = theta;
};

Scene.prototype.render = function(gl, shaderManager)
{
    // Set color shader:
    shaderManager.setAssignment1Shader(gl, this.theta);

    // Render model:
    this.subdividedPolygon.render(gl);
};
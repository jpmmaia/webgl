function AbstractModel()
{
}

AbstractModel.prototype.initialize = function(gl, positionAttribLocation, vertices)
{
    this.positionAttribLocation = positionAttribLocation;

    // Create array buffer to hold vertex data:
    this.vertexBufferId = gl.createBuffer();

    // Create vertex buffer:
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    // Enable vertex attributes array:
    gl.enableVertexAttribArray(positionAttribLocation);

    // Specify the data format and location of attributes in the vertex attributes array:
    gl.vertexAttribPointer(positionAttribLocation, 3, gl.FLOAT, false, 0, 0);
};

AbstractModel.prototype.shutdown = function(gl)
{
    // Disable vertex attributes array:
    gl.disableVertexAttribArray(this.positionAttribLocation);

    // Release the vertex buffer:
    gl.bindBuffer(gl.ARRAY_BUFFER, 0);
    gl.deleteBuffer(this.vertexBufferId);
};

AbstractModel.prototype.render = function(gl)
{
    // Bind vertices buffer:
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBufferId);
};
function Triangle()
{
    this.super = new AbstractModel();
}

Triangle.prototype.initialize = function(gl, positionAttribLocation)
{
    var radius = 0.5;
    var angle1 = Math.PI / 2.0;
    var angle2 = 7.0 * Math.PI / 6.0;
    var angle3 = 11.0 * Math.PI / 6.0;

    this.vertices =
        [
            radius * Math.cos(angle1), radius * Math.sin(angle1), 0.0,
            radius * Math.cos(angle2), radius * Math.sin(angle2), 0.0,
            radius * Math.cos(angle3), radius * Math.sin(angle3), 0.0
        ];

    this.super.initialize(gl, positionAttribLocation, this.vertices);
};

Triangle.prototype.render = function(gl)
{
    this.super.render(gl);

    // Draw triangle:
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 3);
};
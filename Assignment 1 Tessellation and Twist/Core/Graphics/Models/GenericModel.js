function GenericModel()
{
    this.super = new AbstractModel();
}

GenericModel.prototype.initialize = function(gl, positionAttribLocation, vertices)
{
    // Calculate vertex count:
    this.vertexCount = vertices.length / 3;

    this.super.initialize(gl, positionAttribLocation, vertices);
};

GenericModel.prototype.render = function(gl)
{
    this.super.render(gl);

    // Draw model:
    gl.drawArrays(gl.TRIANGLES, 0, this.vertexCount);
};
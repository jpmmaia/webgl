function Square()
{
    this.super = new AbstractModel();
}

Square.prototype.initialize = function(gl, positionAttribLocation)
{
    this.vertices =
    [
        -0.5, -0.5, 0.0,
        0.5, -0.5, 0.0,
        -0.5, 0.5, 0.0,
        -0.5, 0.5, 0.0,
        0.5, -0.5, 0.0,
        0.5, 0.5, 0.0
    ];

    this.super.initialize(gl, positionAttribLocation, this.vertices);
};

Square.prototype.render = function(gl)
{
    this.super.render(gl);

    // Draw square:
    gl.drawArrays(gl.TRIANGLES, 0, 6);
};
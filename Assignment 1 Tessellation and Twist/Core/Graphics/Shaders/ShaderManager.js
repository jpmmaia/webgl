function ShaderManager()
{
}

ShaderManager.prototype.initialize = function(gl)
{
    // Initialize color shader:
    this.colorShader = new ColorShader();
    this.colorShader.initialize(gl);

    // Initialize assignment 1 shader:
    this.assignment1Shader = new Assignment1Shader();
    this.assignment1Shader.initialize(gl);
};

ShaderManager.prototype.setColorShader = function(gl)
{
    this.colorShader.setShader(gl);
};

ShaderManager.prototype.setAssignment1Shader = function(gl, theta)
{
    this.assignment1Shader.setShader(gl);

    this.assignment1Shader.setShaderParameters(gl, theta);
};
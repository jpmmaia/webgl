function ColorShader()
{
    this.super = new AbstractShader();
}

ColorShader.prototype.initialize = function(gl)
{
    this.super.initialize(gl, "ColorVertexShader", "ColorFragmentShader");

    // Get attributes location:
    this.position = gl.getAttribLocation(this.super.program, "position");
};

ColorShader.prototype.setShader = function(gl)
{
    this.super.setShader(gl);
};
function Assignment1Shader()
{
    this.super = new AbstractShader();
}

Assignment1Shader.prototype.initialize = function(gl)
{
    this.super.initialize(gl, "Assignment1VertexShader", "Assignment1FragmentShader");

    // Get attributes location:
    this.position = gl.getAttribLocation(this.super.program, "position");

    // Get uniforms location:
    this.theta = gl.getUniformLocation(this.super.program, "theta");
};

Assignment1Shader.prototype.setShader = function(gl)
{
    this.super.setShader(gl);
};

Assignment1Shader.prototype.setShaderParameters = function(gl, theta)
{
    gl.uniform1f(this.theta, theta);
};